__author__    = "I-Huan CHIU"
__email__     = "ichiu@chem.sci.osaka-u.ac.jp"
__created__   = "2019-11-08"
__copyright__ = "Copyright 2019 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import sys,os,random,math,ROOT
from root_numpy import hist2array, array2hist, tree2array
import numpy as np
ROOT.gROOT.SetBatch(1)
import argparse
import math
from color import SetMyPalette
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()

if __name__=="__main__":
   mom_list = ["25MeV", "30MeV"]
   hmom=[ROOT.TH1D("25MeV","25MeV",21,0,21),ROOT.TH1D("30MeV","30MeV",21,0,21)]
   for j, imom in enumerate(mom_list):
      for i in range(1,21):
         fname = "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/muonstop_build/{0}/Output_{1}.root".format(imom,i)
         fint = ROOT.TFile(fname,"read")
         tree = fint.Get("tree")
         tree.Draw("muSampleEndPolZ >> h1(1000,0,1)")
         h1=ROOT.gDirectory.Get("h1") 
         h1.SetDirectory(0)
         hmom[j].Fill(i,h1.GetRMS())

   leg = ROOT.TLegend(.7,.7,.95,.95)
   leg.SetFillColorAlpha(0,0)
   leg.SetLineColorAlpha(0,0)
   leg.SetBorderSize(0)
   leg.AddEntry(hmom[0], "25 MeV", "p")
   leg.AddEntry(hmom[1], "30 MeV", "p")
   hmom[0].GetXaxis().SetRangeUser(1,20)
   hmom[1].GetXaxis().SetRangeUser(1,20)
   hmom[0].SetStats(0)
   hmom[1].SetStats(0)
   hmom[0].SetMarkerColor(2)
   hmom[1].SetMarkerColor(4)
   hmom[0].SetLineColor(2)
   hmom[1].SetLineColor(4)
   hmom[0].SetMarkerSize(2)
   hmom[1].SetMarkerSize(2)
   hmom[1].SetMaximum(0.25)
   hmom[1].SetMinimum(0.)
   hmom[1].SetTitle(";Cu thickness[10 #mum]; Stopping depth [mm]")
   hmom[1].GetXaxis().CenterTitle(); hmom[1].GetYaxis().CenterTitle();
   cc = ROOT.TCanvas("cc","cc",1500,1000) 
   cc.cd()
   hmom[1].Draw("hist p l");
   hmom[0].Draw("hist p l same"); 
   leg.Draw("same")
   cc.Print("/Users/chiu.i-huan/Desktop/temp_output/cv_arch_stop.pdf")
      
