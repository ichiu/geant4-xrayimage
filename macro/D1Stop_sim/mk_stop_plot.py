import os,ROOT
from ROOT import *
from root_numpy import hist2array, array2hist, tree2array
import numpy as np
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()

def mk2dplot(filename):
   f1=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/d1stop_build/"+filename,"read")
   tree=f1.Get("tree")
   muon_list=["32.4","40","43.2","46.4","50.8"]
   for i in range(5):
      num_list = [0,0,0,0,0]
      back_sca=0
      tree.Draw("muFinalPolX:muFinalPolZ >> h{0}(1600,-150,10,600,-30,30)".format(i),"muInitPindex == {0}".format(i),"")
      h1=ROOT.gDirectory.Get("h{0}".format(i))
      
      h1.SetTitle(";Position Z [mm]; Position X [mm]")
      h2=h1.Clone()
      _array=hist2array(h1) 
      _array[np.where(_array == 0)]=0.0000000000000
      array2hist(_array,h2)

      c2=ROOT.TCanvas("c2_{}".format(i),"c2_{}".format(i),1200,1200)
      c2.cd()
      #ROOT.gPad.SetLogz(1)
      #ROOT.gStyle.SetPalette(53)
      ROOT.gPad.SetRightMargin(0.15)
      h2.Draw("colz")
      c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_d1stop_{}.png".format(muon_list[i]))
  
      num_list[0] = tree.GetEntries("MuonStop_VolumeID == 1 && muInitPindex == {} && muFinalPolZ > -150".format(i))#sample
      num_list[1] = tree.GetEntries("MuonStop_VolumeID == 3 && muFinalPolZ < 0 && muFinalPolZ >= -55 && muInitPindex == {}".format(i))#sn
      num_list[2] = tree.GetEntries("MuonStop_VolumeID == 3 && muFinalPolZ < -55 && muFinalPolZ >= -135 && muInitPindex == {}".format(i))#pb
      num_list[3] = tree.GetEntries("MuonStop_VolumeID == -1 && muInitPindex == {} && muFinalPolZ > -150 ".format(i))#colli
      num_list[4] = tree.GetEntries("MuonStop_VolumeID != -1 && MuonStop_VolumeID != 1 && MuonStop_VolumeID != 3 && muInitPindex == {} && muFinalPolZ > -150".format(i))#other
      back_sca = tree.GetEntries("MuonStop_VolumeID == 3 && hit_sample == 1") #back scattering 
      print(num_list, back_sca)
      print(tree.GetEntries("MuonStop_VolumeID == 3 && muInitPindex == {}".format(i))/tree.GetEntries("MuonStop_VolumeID == 1 && muInitPindex == {}".format(i)))

      tree.Draw("muFinalPolX:muFinalPolZ >> hz{0}(80,-1,3,600,-30,30)".format(i),"muInitPindex == {0}".format(i),"")
      hz1=ROOT.gDirectory.Get("hz{0}".format(i))
      hz1.SetTitle(";Position Z [mm]; Position X [mm]")
      c3=ROOT.TCanvas("c3_{}".format(i),"c3_{}".format(i),1200,1200)
      c3.cd()
      ROOT.gPad.SetRightMargin(0.15)
      hz1.Draw("colz")
      c3.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_d1stop_zoomin_{}.png".format(muon_list[i]))


   
def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

if __name__=="__main__":
   mk2dplot(filename="Output_1_s7mm.root")


