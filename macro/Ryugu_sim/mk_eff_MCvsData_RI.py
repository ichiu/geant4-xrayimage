"""
for detector eff. calculation
Ryugu fitting result 
"""
import os,ROOT,math
from array import array
import numpy as np
from ROOT import *
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gErrorIgnoreLevel = ROOT.kFatal

nbins=int(800)

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

if __name__=="__main__":
   # === read data ===
   fdata=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_result_RI.root","read")
   fmc_ch2=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ri_osaka_build/Output_RI_gamma_CH2_tunning.root","read")
   fmc_ch3=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ri_osaka_build/Output_RI_gamma_CH3_tunning.root","read")
   tree_data=fdata.Get("tree")
   tree_mc2=fmc_ch2.Get("tree")
   tree_mc3=fmc_ch3.Get("tree")

   # === make fig. ===
   g2,g3 = ROOT.TGraph(),ROOT.TGraph()
   index2,index3=0,0
   for ie in range(tree_data.GetEntries()):
      tree_data.GetEntry(ie)
      if tree_data.ch == 2: 
         g2.SetPoint(index2,tree_data.peak,tree_data.eff)
         index2+=1
      if tree_data.ch == 3: 
         g3.SetPoint(index3,tree_data.peak,tree_data.eff)
         index3+=1

   h02=fmc_ch2.Get("h1_init_energy")
   h03=fmc_ch3.Get("h1_init_energy")
   h02.Rebin(int(8000/nbins))
   h03.Rebin(int(8000/nbins))
   tree_mc2.Draw("Hit_Energy_Reso*1000 >> h2({},0,200)".format(nbins),"Det_ID == 2","")
   tree_mc3.Draw("Hit_Energy_Reso*1000 >> h3({},0,200)".format(nbins),"Det_ID == 3","")
   h2=ROOT.gDirectory.Get("h2"); h2.Divide(h02)
   h3=ROOT.gDirectory.Get("h3"); h3.Divide(h03)
 
   max_content=0.0015
   h2.SetMaximum(max_content)
   h3.SetMaximum(max_content)
   g2.SetTitle(";Energy [keV]; Eff./25 eV")
   g3.SetTitle(";Energy [keV]; Eff./25 eV")

   g2.SetMarkerColor(4)
   g3.SetMarkerColor(4)
   h2.SetLineColor(2)
   h3.SetLineColor(2)

   leg = ROOT.TLegend(.65,.5,.8,.75)
   leg.SetFillColor(0)
   leg.SetLineColor(0)
   leg.SetBorderSize(0)
   leg.AddEntry(h2, "MC" , "l")
   leg.AddEntry(g2, "Data" , "p")

   c2=ROOT.TCanvas("c2","c2",1000,800)
   h2.Draw("hist")
   g2.Draw("p same")
   leg.Draw("same")
   c2.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch2_RIcenter_t2.pdf")
   c3=ROOT.TCanvas("c3","c3",1000,800)
   h3.Draw("hist")
   g3.Draw("p same")
   c3.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch3_RIcenter_t2.pdf")
