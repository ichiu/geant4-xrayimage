"""
for detector eff. calculation
Ryugu fitting result 
"""
import os,ROOT,math
from array import array
import numpy as np
from ROOT import gROOT, gStyle
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gErrorIgnoreLevel = ROOT.kFatal

#gROOT.SetBatch(1)
#gROOT.SetStyle('Plain')
#gStyle.SetOptStat(False)
#gStyle.SetEndErrorSize(0)
#gStyle.SetFrameFillColor(0)
#gStyle.SetCanvasColor(0)
#gStyle.SetPadBorderSize(0)

nbins=int(800)

def getLatex():
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

if __name__=="__main__":
   # === read data ===
   fdata=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_sum.root","read")
   fmc=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_gamma_tunning_final_kapton.root","read")
   fmc_ri=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_sum_MC.root","read")
   #fmc=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_gamma_Ryugu.root","read")
   tree_data=fdata.Get("tree")
   tree_mc=fmc.Get("tree")
   tree_mc_ri=fmc_ri.Get("tree")

   # === make fig. ===
   g1,g2,g3,g4,g5,g6 = ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()
   index1,index2,index3,index4,index5,index6=0,0,0,0,0,0
   for ie in range(tree_data.GetEntries()):
      tree_data.GetEntry(ie)
      if tree_data.ch == 1: 
         g1.SetPoint(index1,tree_data.peak,tree_data.eff)
         index1+=1
      if tree_data.ch == 2: 
         g2.SetPoint(index2,tree_data.peak,tree_data.eff)
         index2+=1
      if tree_data.ch == 3: 
         g3.SetPoint(index3,tree_data.peak,tree_data.eff)
         index3+=1
      if tree_data.ch == 4: 
         g4.SetPoint(index4,tree_data.peak,tree_data.eff)
         index4+=1
      if tree_data.ch == 5: 
         g5.SetPoint(index5,tree_data.peak,tree_data.eff)
         index5+=1
      if tree_data.ch == 6: 
         g6.SetPoint(index6,tree_data.peak,tree_data.eff)
         index6+=1

   g1_mc,g2_mc,g3_mc,g4_mc,g5_mc,g6_mc = ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()
   index1,index2,index3_mc,index4,index5,index6=0,0,0,0,0,0
   for ie in range(tree_mc_ri.GetEntries()):
      tree_mc_ri.GetEntry(ie)
      if tree_mc_ri.ch == 1: 
         g1_mc.SetPoint(index1,tree_mc_ri.peak,tree_mc_ri.eff)
         index1+=1
      if tree_mc_ri.ch == 2: 
         g2_mc.SetPoint(index2,tree_mc_ri.peak,tree_mc_ri.eff)
         index2+=1
      if tree_mc_ri.ch == 3: 
         g3_mc.SetPoint(index3,tree_mc_ri.peak,tree_mc_ri.eff)
         index3+=1
      if tree_mc_ri.ch == 4: 
         g4_mc.SetPoint(index4,tree_mc_ri.peak,tree_mc_ri.eff)
         index4+=1
      if tree_mc_ri.ch == 5: 
         g5_mc.SetPoint(index5,tree_mc_ri.peak,tree_mc_ri.eff)
         index5+=1
      if tree_mc_ri.ch == 6: 
         g6_mc.SetPoint(index6,tree_mc_ri.peak,tree_mc_ri.eff)
         index6+=1

   h0=fmc.Get("h1_init_energy")
   h0.Rebin(int(8000/nbins))
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h1({},0,200)".format(nbins),"Det_ID == 1","")
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h2({},0,200)".format(nbins),"Det_ID == 2","")
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h3({},0,200)".format(nbins),"Det_ID == 3","")
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h4({},0,200)".format(nbins),"Det_ID == 4","")
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h5({},0,200)".format(nbins),"Det_ID == 5","")
   tree_mc.Draw("Hit_Energy_Reso*1000 >> h6({},0,200)".format(nbins),"Det_ID == 6","")
   h1=ROOT.gDirectory.Get("h1"); h1.Divide(h0)
   h2=ROOT.gDirectory.Get("h2"); h2.Divide(h0)
   h3=ROOT.gDirectory.Get("h3"); h3.Divide(h0)
   h4=ROOT.gDirectory.Get("h4"); h4.Divide(h0)
   h5=ROOT.gDirectory.Get("h5"); h5.Divide(h0)
   h6=ROOT.gDirectory.Get("h6"); h6.Divide(h0)
 
   # === fit ===
   func_name="pol7"
   #g1.Fit(func_name,"q"); f1_data = g1.GetFunction(func_name);
   #g2.Fit(func_name,"q"); f2_data = g2.GetFunction(func_name);
   #g3.Fit(func_name,"q"); f3_data = g3.GetFunction(func_name);
   #g4.Fit(func_name,"q"); f4_data = g4.GetFunction(func_name);
   #g5.Fit(func_name,"q"); f5_data = g5.GetFunction(func_name);
   #g6.Fit(func_name,"q"); f6_data = g6.GetFunction(func_name);
   h1.Fit(func_name,"q"); f1_mc = h1.GetFunction(func_name);
   h2.Fit(func_name,"q"); f2_mc = h2.GetFunction(func_name);
   h3.Fit(func_name,"q"); f3_mc = h3.GetFunction(func_name);
   h4.Fit(func_name,"q"); f4_mc = h4.GetFunction(func_name);
   h5.Fit(func_name,"q"); f5_mc = h5.GetFunction(func_name);
   h6.Fit(func_name,"q"); f6_mc = h6.GetFunction(func_name);

   # === store ===
   fout = ROOT.TFile("./output_eff_plots_Ryugu.root","recreate")
   fout.cd()
   #f1_data.SetName("f1_data");f1_data.Write()
   #f2_data.SetName("f2_data");f2_data.Write()
   #f3_data.SetName("f3_data");f3_data.Write()
   #f4_data.SetName("f4_data");f4_data.Write()
   #f5_data.SetName("f5_data");f5_data.Write()
   #f6_data.SetName("f6_data");f6_data.Write()
   f1_mc.SetName("f1_mc");f1_mc.Write()
   f2_mc.SetName("f2_mc");f2_mc.Write()
   f3_mc.SetName("f3_mc");f3_mc.Write()
   f4_mc.SetName("f4_mc");f4_mc.Write()
   f5_mc.SetName("f5_mc");f5_mc.Write()
   f6_mc.SetName("f6_mc");f6_mc.Write()
   h1.SetName("ch1_mc");h1.Write()
   h2.SetName("ch2_mc");h2.Write()
   h3.SetName("ch3_mc");h3.Write()
   h4.SetName("ch4_mc");h4.Write()
   h5.SetName("ch5_mc");h5.Write()
   h6.SetName("ch6_mc");h6.Write()
   g1.SetName("ch1_data");g1.Write()
   g2.SetName("ch2_data");g2.Write()
   g3.SetName("ch3_data");g3.Write()
   g4.SetName("ch4_data");g4.Write()
   g5.SetName("ch5_data");g5.Write()
   g6.SetName("ch6_data");g6.Write()
   g1_mc.SetName("ch1_mc_ri");g1_mc.Write()
   g2_mc.SetName("ch2_mc_ri");g2_mc.Write()
   g3_mc.SetName("ch3_mc_ri");g3_mc.Write()
   g4_mc.SetName("ch4_mc_ri");g4_mc.Write()
   g5_mc.SetName("ch5_mc_ri");g5_mc.Write()
   g6_mc.SetName("ch6_mc_ri");g6_mc.Write()

   # === plot setting===
#   max_content=0.001*(int(6800/nbins))
#   f1_mc.SetMaximum(max_content)
#   f2_mc.SetMaximum(max_content)
#   f3_mc.SetMaximum(max_content)
#   f4_mc.SetMaximum(max_content)
#   f5_mc.SetMaximum(max_content)
#   f6_mc.SetMaximum(max_content)
#   f1_mc.SetTitle(";Energy [keV]; Eff./25 eV")
#   f2_mc.SetTitle(";Energy [keV]; Eff./25 eV")
#   f3_mc.SetTitle(";Energy [keV]; Eff./25 eV")
#   f4_mc.SetTitle(";Energy [keV]; Eff./25 eV")
#   f5_mc.SetTitle(";Energy [keV]; Eff./25 eV")
#   f6_mc.SetTitle(";Energy [keV]; Eff./25 eV")
   max_content=0.0015
   h1.SetMaximum(max_content)
   h2.SetMaximum(max_content)
   h3.SetMaximum(max_content)
   h4.SetMaximum(max_content)
   h5.SetMaximum(max_content)
   h6.SetMaximum(max_content)
   g1.SetTitle(";Energy [keV]; Eff./25 eV")
   g2.SetTitle(";Energy [keV]; Eff./25 eV")
   g3.SetTitle(";Energy [keV]; Eff./25 eV")
   g4.SetTitle(";Energy [keV]; Eff./25 eV")
   g5.SetTitle(";Energy [keV]; Eff./25 eV")
   g6.SetTitle(";Energy [keV]; Eff./25 eV")

#   f1_data.SetLineColor(2)
#   f2_data.SetLineColor(2)
#   f3_data.SetLineColor(2)
#   f4_data.SetLineColor(2)
#   f5_data.SetLineColor(2)
#   f6_data.SetLineColor(2)
   f1_mc.SetLineColorAlpha(1,0.5)
   f2_mc.SetLineColorAlpha(1,0.5)
   f3_mc.SetLineColorAlpha(1,0.5)
   f4_mc.SetLineColorAlpha(1,0.5)
   f5_mc.SetLineColorAlpha(1,0.5)
   f6_mc.SetLineColorAlpha(1,0.5)
   g1.SetMarkerColor(4)
   g2.SetMarkerColor(4)
   g3.SetMarkerColor(4)
   g4.SetMarkerColor(4)
   g5.SetMarkerColor(4)
   g6.SetMarkerColor(4)
   g1_mc.SetMarkerColor(ROOT.kSpring-1)
   g2_mc.SetMarkerColor(ROOT.kSpring-1)
   g3_mc.SetMarkerColor(ROOT.kSpring-1)
   g4_mc.SetMarkerColor(ROOT.kSpring-1)
   g5_mc.SetMarkerColor(ROOT.kSpring-1)
   g6_mc.SetMarkerColor(ROOT.kSpring-1)
   h1.SetLineColorAlpha(2,0.7)
   h2.SetLineColorAlpha(2,0.7)
   h3.SetLineColorAlpha(2,0.7)
   h4.SetLineColorAlpha(2,0.7)
   h5.SetLineColorAlpha(2,0.7)
   h6.SetLineColorAlpha(2,0.7)
   h1.SetLineWidth(1)
   h2.SetLineWidth(1)
   h3.SetLineWidth(1)
   h4.SetLineWidth(1)
   h5.SetLineWidth(1)
   h6.SetLineWidth(1)

   leg = ROOT.TLegend(.65,.65,.8,.85)
   leg.SetFillColorAlpha(0,0)
   leg.SetLineColorAlpha(0,0)
   leg.SetBorderSize(0)
   #leg.AddEntry(f1_data, "Data" , "l")
   #leg.AddEntry(f1_mc, "MC" , "l")
   leg.AddEntry(h1, "MC" , "l")
   leg.AddEntry(g1_mc, "MC RI" , "p")
   leg.AddEntry(g1, "Data" , "p")

   # === plotting fline ===
#   draw_type="C"
#   Ge_diameter = np.array([11.3-0.5*2,11.5-0.5*2,11.2-0.5*2,11.3-0.5*2,11.0-0.5*2,11.4-0.5*2])
#   dead_layer = np.array([0.5,0.5,0.5,0.5,0.5,0.5])
#   c1=ROOT.TCanvas("c1","c1",1000,800)
#   f1_mc.Draw(draw_type)
#   f1_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c1.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch1.pdf")
#   print("Ratio@75 keV:{:.2f}".format(f1_data.Eval(75)/f1_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[0], "New dia:{:.2f}".format(Ge_diameter[0]*math.sqrt(f1_data.Eval(75)/f1_mc.Eval(75))))
#   c2=ROOT.TCanvas("c2","c2",1000,800)
#   f2_mc.Draw(draw_type)
#   f2_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c2.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch2.pdf")
#   print("Ratio@75 keV:{:.2f}".format(f2_data.Eval(75)/f2_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[1], "New dia:{0:.2f}".format(Ge_diameter[1]*math.sqrt(f2_data.Eval(75)/f2_mc.Eval(75))))
#   c3=ROOT.TCanvas("c3","c3",1000,800)
#   f3_mc.Draw(draw_type)
#   f3_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c3.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch3.pdf")
#   print("Ratio@75 keV:{0:.2f}".format(f3_data.Eval(75)/f3_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[2], "New dia:{0:.2f}".format(Ge_diameter[2]*math.sqrt(f3_data.Eval(75)/f3_mc.Eval(75))))
#   c4=ROOT.TCanvas("c4","c4",1000,800)
#   f4_mc.Draw(draw_type)
#   f4_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c4.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch4.pdf")
#   print("Ratio@75 keV:{0:.2f}".format(f4_data.Eval(75)/f4_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[3], "New dia:{0:.2f}".format(Ge_diameter[3]*math.sqrt(f4_data.Eval(75)/f4_mc.Eval(75))))
#   c5=ROOT.TCanvas("c5","c5",1000,800)
#   f5_mc.Draw(draw_type)
#   f5_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c5.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch5.pdf")
#   print("Ratio@75 keV:{0:.2f}".format(f5_data.Eval(75)/f5_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[4], "New dia:{0:.2f}".format(Ge_diameter[4]*math.sqrt(f5_data.Eval(75)/f5_mc.Eval(75))))
#   c6=ROOT.TCanvas("c6","c6",1000,800)
#   f6_mc.Draw(draw_type)
#   f6_data.Draw(draw_type+" same")
#   leg.Draw("same")
#   c6.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch6.pdf")
#   print("Ratio@75 keV:{0:.2f}".format(f6_data.Eval(75)/f6_mc.Eval(75)), "Old dia(+1 mm dead layer):", Ge_diameter[5], "New dia:{0:.2f}".format(Ge_diameter[5]*math.sqrt(f6_data.Eval(75)/f6_mc.Eval(75))))


#   c1=ROOT.TCanvas("c1","c1",1000,800)
#   h1.Draw("hist")
#   g1.Draw("p same")
#   leg.Draw("same")
#   c1.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch1.pdf")
#   c2=ROOT.TCanvas("c2","c2",1000,800)
#   h2.Draw("hist")
#   g2.Draw("p same")
#   c2.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch2.pdf")
#   c3=ROOT.TCanvas("c3","c3",1000,800)
#   h3.Draw("hist")
#   g3.Draw("p same")
#   c3.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch3.pdf")
#   c4=ROOT.TCanvas("c4","c4",1000,800)
#   h4.Draw("hist")
#   g4.Draw("p same")
#   c4.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch4.pdf")
#   c5=ROOT.TCanvas("c5","c5",1000,800)
#   h5.Draw("hist")
#   g5.Draw("p same")
#   c5.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch5.pdf")
#   c6=ROOT.TCanvas("c6","c6",1000,800)
#   h6.Draw("hist")
#   g6.Draw("p same")
#   c6.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_ch6.pdf")

   latex = getLatex()
   cv1=ROOT.TCanvas("cv1","cv1",2000,1600)
   cv1.Divide(3,2)
   cv1.cd(1)
   h1.Draw("hist")
   g1.Draw("p same")
   g1_mc.Draw("p same")
   f1_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH1")
   leg.Draw("same")
   cv1.cd(2)
   h2.Draw("hist")
   g2.Draw("p same")
   g2_mc.Draw("p same")
   f2_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH2")
   cv1.cd(3)
   h3.Draw("hist")
   g3.Draw("p same")
   g3_mc.Draw("p same")
   f3_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH3")
   cv1.cd(4)
   h4.Draw("hist")
   g4.Draw("p same")
   g4_mc.Draw("p same")
   f4_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH4")
   cv1.cd(5)
   h5.Draw("hist")
   g5.Draw("p same")
   g5_mc.Draw("p same")
   f5_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH5")
   cv1.cd(6)
   h6.Draw("hist")
   g6.Draw("p same")
   g6_mc.Draw("p same")
   f6_mc.Draw("C same")
   latex.DrawLatex(0.5,0.85,"CH6")
   cv1.SaveAs("/Users/chiu.i-huan/Desktop/c_data_VS_mc_fline_MCRI.pdf")
