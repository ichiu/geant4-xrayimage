"""
This module shows the main functions.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2023-02-02"
__copyright__ = "Copyright 2023 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import os,sys,ROOT,argparse
from datetime import date
today = date.today()

def main(args):
    sample_name_list=["Ryugu","Murray","Yamato","Tagish","Orgueil","Fe"]
    fint = ROOT.TFile(args.inputFolder,"read")
    for sname in sample_name_list:
       for i in range(6):
          hname = "hist_CH{0}_{1}".format(i+1, sname)
          h = fint.Get(hname)
   
          print_latex = []
          print_latex.append("# PHA DATA for Ryugu Simulation")
          print_latex.append("# Date: {}".format(today.strftime("%B %d, %Y")))
          print_latex.append("# Root file: {}".format(fint.GetName()))
          print_latex.append("# Histogram: {}".format(hname))
          print_latex.append("# PHA data format: binID, time at the bin center, content")
   
          Nbin=h.GetXaxis().GetNbins()
          for i in range(Nbin):
             _center=h.GetBinCenter(i+1)
             _content=h.GetBinContent(i+1)
             print_latex.append("{0}, {1:.4f}, {2}".format(i+1, _center, _content))
   
          outpha=args.inputFolder.replace(".root","_{}.pha".format(hname))
          outpha=outpha.replace("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/","/Users/chiu.i-huan/Desktop/temp_output/")
          with open(outpha, 'w') as f:
             f.write("\n".join(print_latex))
          print("Output file : {}".format(outpha)) 

def resp_2d():
    inputname = "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/Output_Ryugu_resp2dim_0227_sum_5p6B_final.root"
    fint = ROOT.TFile(inputname,"read")

    for i in range(6):
       hname = "resp2dim_ch{}".format(i+1)
       h = fint.Get(hname)
   
       print_latex = []
       print_latex.append("# PHA DATA for Response Simulation")
       print_latex.append("# Date: {}".format(today.strftime("%B %d, %Y")))
       print_latex.append("# Root file: {}".format(fint.GetName()))
       print_latex.append("# Histogram: {}".format(hname))
       #print_latex.append("# PHA data format: Raw (X), Column (Y), content")#Type-1
       print_latex.append("# PHA data format: energy deposit (Y), photon energy (X), content") #Type-2
   
       NbinX=h.GetXaxis().GetNbins()
       NbinY=h.GetYaxis().GetNbins()

       # Type-1
#       print_top = " ,"
#       for j in range(NbinY):
#          _centerY=h.GetYaxis().GetBinCenter(j+1)
#          print_top += "{0:.4f}, ".format(_centerY)
#       print_latex.append(print_top)
#
#       for ix in range(NbinX):
#          _centerX=h.GetXaxis().GetBinCenter(ix+1)
#          print_raw = "{0:.4f}, ".format(_centerX)
#          for j in range(NbinY):
#             _content=h.GetBinContent(ix+1,j+1)
#             print_raw += " {0}, ".format(_content)
#          print_latex.append(print_raw)

       # Type-2
       for ix in range(NbinX):
          for iy in range(NbinY):
             _centerX=h.GetXaxis().GetBinCenter(ix+1)
             _centerY=h.GetYaxis().GetBinCenter(iy+1)
             _content=h.GetBinContent(ix+1,iy+1)
             print_latex.append("{0:.4f}, {1:.4f}, {2}".format(_centerX, _centerY, _content))
   
       outpha=inputname.replace(".root","_CH{}_v2.pha".format(i+1))
       outpha=outpha.replace("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/","/Users/chiu.i-huan/Desktop/temp_output/")
       with open(outpha, 'w') as f:
          f.write("\n".join(print_latex))
       print("Output file : {}".format(outpha)) 

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("--inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/absorption_1d_output_v2.root", help="Input File Name")
    args = parser.parse_args()

#    main(args)
    resp_2d()
