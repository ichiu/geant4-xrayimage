"""
for detector eff. calculation
Ryugu fitting result 
"""
import os,ROOT,ctypes
from array import array
from ROOT import *
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()


def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def plot_eff_ryugu():
   # === read data ===
   f1=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_sum.root","read")
   tree=f1.Get("tree")

   # === make fig. ===
   g1,g2,g3,g4,g5,g6 = ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()
   index1,index2,index3,index4,index5,index6=0,0,0,0,0,0
   for ie in range(tree.GetEntries()):
      tree.GetEntry(ie)
      if tree.ch == 1: 
         g1.SetPoint(index1,tree.peak,tree.eff)
         index1+=1
      if tree.ch == 2: 
         g2.SetPoint(index2,tree.peak,tree.eff)
         index2+=1
      if tree.ch == 3: 
         g3.SetPoint(index3,tree.peak,tree.eff)
         index3+=1
      if tree.ch == 4: 
         g4.SetPoint(index4,tree.peak,tree.eff)
         index4+=1
      if tree.ch == 5: 
         g5.SetPoint(index5,tree.peak,tree.eff)
         index5+=1
      if tree.ch == 6: 
         g6.SetPoint(index6,tree.peak,tree.eff)
         index6+=1

   #TODO define fitting function 
   # === mk function ===
   func_name="total"
   par0,par1,par2,par3,par4,par5,par6 = map(ctypes.c_double, (0,0,0,0,0,0,0))
   _func1=ROOT.TF1("_func1","expo",10,40)
   _func2=ROOT.TF1("_func2","pol0",40,90)
   _func3=ROOT.TF1("_func3","expo",90,180)
   total=ROOT.TF1(func_name,"expo(0)+pol0(2)+expo(3)",10,180)
   total.SetLineColor(2)

   # === plot & fit ===
   g1.Fit(_func1,"R")
   g1.Fit(_func2,"R+")
   g1.Fit(_func3,"R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   total.SetParameters(par0,par1,par3,par4,par5)
#   total.FixParameter(0,par0)
#   total.FixParameter(1,par1)
#   total.FixParameter(2,par3)
#   total.FixParameter(3,par4)
#   total.FixParameter(4,par5)
   g1.Fit(total,"R")
   f1 = total
   f1.SetLineColor(2)
   c1=ROOT.TCanvas("c1","c1",1000,800)
   g1.Draw("AP")
   c1.SaveAs("/Users/chiu.i-huan/Desktop/c_ch1_eff.pdf")

   g2.Fit("_func1","R")
   g2.Fit("_func2","R+")
   g2.Fit("_func3","R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par2=_func1.GetParameter(2)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   par6=_func3.GetParameter(2)
   total.SetParameters(par0,par1,par2,par3,par4,par5,par6)
   g2.Fit(total,"R")
   f2 = total
   f2.SetLineColor(2)
   c2=ROOT.TCanvas("c2","c2",1000,800)
   g2.Draw("AP")
   c2.SaveAs("/Users/chiu.i-huan/Desktop/c_ch2_eff.pdf")

   g3.Fit("_func1","R")
   g3.Fit("_func2","R+")
   g3.Fit("_func3","R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par2=_func1.GetParameter(2)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   par6=_func3.GetParameter(2)
   total.SetParameters(par0,par1,par2,par3,par4,par5,par6)
   f3 = total
   f3.SetLineColor(2)
   c3=ROOT.TCanvas("c3","c3",1000,800)
   g3.Draw("AP")
   c3.SaveAs("/Users/chiu.i-huan/Desktop/c_ch3_eff.pdf")

   g4.Fit("_func1","R")
   g4.Fit("_func2","R+")
   g4.Fit("_func3","R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par2=_func1.GetParameter(2)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   par6=_func3.GetParameter(2)
   total.SetParameters(par0,par1,par2,par3,par4,par5,par6)
   f4 = total
   f4.SetLineColor(2)
   c4=ROOT.TCanvas("c4","c4",1000,800)
   g4.Draw("AP")
   c4.SaveAs("/Users/chiu.i-huan/Desktop/c_ch4_eff.pdf")

   g5.Fit("_func1","R")
   g5.Fit("_func2","R+")
   g5.Fit("_func3","R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par2=_func1.GetParameter(2)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   par6=_func3.GetParameter(2)
   total.SetParameters(par0,par1,par2,par3,par4,par5,par6)
   f5 = total
   f5.SetLineColor(2)
   c5=ROOT.TCanvas("c5","c5",1000,800)
   g5.Draw("AP")
   c5.SaveAs("/Users/chiu.i-huan/Desktop/c_ch5_eff.pdf")

   g6.Fit("_func1","R")
   g6.Fit("_func2","R+")
   g6.Fit("_func3","R+")
   par0=_func1.GetParameter(0)
   par1=_func1.GetParameter(1)
   par2=_func1.GetParameter(2)
   par3=_func2.GetParameter(0)
   par4=_func3.GetParameter(0)
   par5=_func3.GetParameter(1)
   par6=_func3.GetParameter(2)
   total.SetParameters(par0,par1,par2,par3,par4,par5,par6)
   f6 = total
   f6.SetLineColor(2)
   c6=ROOT.TCanvas("c6","c6",1000,800)
   g6.Draw("AP")
   c6.SaveAs("/Users/chiu.i-huan/Desktop/c_ch6_eff.pdf")

   # === store ===
   fout = ROOT.TFile("./output_eff_plots_Ryugu.root","recreate")
   fout.cd()
   g1.SetName("ch1");g1.Write();f1.SetName("f1");f1.Write()
   g2.SetName("ch2");g2.Write();f2.SetName("f2");f2.Write()
   g3.SetName("ch3");g3.Write();f3.SetName("f3");f3.Write()
   g4.SetName("ch4");g4.Write();f4.SetName("f4");f4.Write()
   g5.SetName("ch5");g5.Write();f5.SetName("f5");f5.Write()
   g6.SetName("ch6");g6.Write();f6.SetName("f6");f6.Write()

   # === check ===
   c0=ROOT.TCanvas("c0","c0",1000,800)
   f1.SetLineColor(1)
   f2.SetLineColor(2)
   f3.SetLineColor(3)
   f4.SetLineColor(4)
   f5.SetLineColor(5)
   f6.SetLineColor(6)
   leg = ROOT.TLegend(.35,.25,.5,.5)
   leg.SetFillColor(0)
   leg.SetLineColor(0)
   leg.SetBorderSize(0)
   leg.AddEntry(f1, "ch1" , "l")
   leg.AddEntry(f2, "ch2" , "l")
   leg.AddEntry(f3, "ch3" , "l")
   leg.AddEntry(f4, "ch4" , "l")
   leg.AddEntry(f5, "ch5" , "l")
   leg.AddEntry(f6, "ch6" , "l")
   draw_type="C"
   f1.SetMaximum(0.0015)
   f1.Draw(draw_type);f2.Draw(draw_type+"same");f3.Draw(draw_type+"same");
   f4.Draw(draw_type+"same");f5.Draw(draw_type+"same");f6.Draw(draw_type+"same");
   leg.Draw("same")
   c0.SaveAs("/Users/chiu.i-huan/Desktop/c_combined_6ch.pdf")

def plot_eff_ri(): 
   # === read data ===
   f1=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_result_RI.root","read")
   tree=f1.Get("tree")

   # === make fig. ===
   g2,g3 = ROOT.TGraph(),ROOT.TGraph()
   index2,index3=0,0
   for ie in range(tree.GetEntries()):
      tree.GetEntry(ie)
      if tree.ch == 2: 
         g2.SetPoint(index2,tree.peak,tree.eff)
         index2+=1
      if tree.ch == 3: 
         g3.SetPoint(index3,tree.peak,tree.eff)
         index3+=1
 
   # === plot & fit ===
   func_name="pol3"

   g2.Fit(func_name,"q"); f2 = g2.GetFunction(func_name);
   f2.SetLineColor(2)
   c2=ROOT.TCanvas("c2","c2",1000,800)
   g2.Draw("AP")
   c2.SaveAs("/Users/chiu.i-huan/Desktop/c_ch2_eff_ri.pdf")

   g3.Fit(func_name,"q"); f3 = g3.GetFunction(func_name);
   f3.SetLineColor(2)
   c3=ROOT.TCanvas("c3","c3",1000,800)
   g3.Draw("AP")
   c3.SaveAs("/Users/chiu.i-huan/Desktop/c_ch3_eff_ri.pdf")

   # === store ===
   fout = ROOT.TFile("./output_eff_plots_Ryugu.root","update")
   fout.cd()
   g2.SetName("ch2_ri");g2.Write();f2.SetName("f2_ri");f2.Write()
   g3.SetName("ch3_ri");g3.Write();f3.SetName("f3_ri");f3.Write()

   # === check ===
   c0=ROOT.TCanvas("c0","c0",1000,800)
   f2.SetLineColor(2)
   f3.SetLineColor(3)
   leg = ROOT.TLegend(.35,.25,.5,.5)
   leg.SetFillColor(0)
   leg.SetLineColor(0)
   leg.SetBorderSize(0)
   leg.AddEntry(f2, "ch2" , "l")
   leg.AddEntry(f3, "ch3" , "l")
   draw_type="C"
   f2.SetMaximum(0.0015)
   f2.Draw(draw_type+"same");f3.Draw(draw_type+"same");
   leg.Draw("same")
   c0.SaveAs("/Users/chiu.i-huan/Desktop/c_combined_6ch_ri.pdf")

if __name__=="__main__":
   plot_eff_ryugu()
#   plot_eff_ri()
