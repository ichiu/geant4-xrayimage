"""
check stoppong depth of muon in Ryugu sample
"""
import os,ROOT
from array import array
from ROOT import *
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

def getLatex():
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def mk_depth():
   f0 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu_SampleOnly.root","read")
   #f1 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu_old.root","read")
   f1 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu_new_29.root","read")
   f2 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu_new.root","read")

   t0=f0.Get("tree")
   t1=f1.Get("tree")
   t2=f2.Get("tree")
   t0.Draw("muSampleEndPolZ >> h0(1400,0,1.4)","muSampleEndPolZ != -1000","")
   t1.Draw("muSampleEndPolZ >> h1(1400,0,1.4)","muSampleEndPolZ != -1000","")
   t2.Draw("muSampleEndPolZ >> h2(1400,0,1.4)","muSampleEndPolZ != -1000","")
   h0=ROOT.gDirectory.Get("h0")
   h1=ROOT.gDirectory.Get("h1")
   h2=ROOT.gDirectory.Get("h2")
   h0.SetStats(0)
   h1.SetStats(0)
   h2.SetStats(0)
   h0.Scale(1./10000)
   h1.Scale(1./10000)
   h2.Scale(1./10000)
   h2.SetTitle("Stopping position;Depth [mm]; Fraction % [/1 #mum]")
   h0.SetLineColor(1)
   h1.SetLineColor(2)
   h2.SetLineColor(4)
   
   leg = ROOT.TLegend(.2,.7,.45,.95)
   leg.SetFillColorAlpha(0,0)
   leg.SetLineColorAlpha(0,0)
   leg.SetBorderSize(0)
   leg.AddEntry(h0, "0", "l") 
   leg.AddEntry(h2, "27", "l") 
   leg.AddEntry(h1, "29.2", "l") 
   
   cv=ROOT.TCanvas("cv","cv",1200,1200)
   cv.cd()
   h2.Draw("hist")
   h0.Draw("hist same")
   h1.Draw("hist same")
   leg.Draw("same")
   cv.Print("/Users/chiu.i-huan/Desktop/temp_output/Ryugu_stop_29.pdf")

def mk_absor():
    """
    only fornt and back
    """
    f0 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu2_shallow.root","read")
    f1 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu2_deep.root","read")
    f2 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu1_shallow.root","read")
    f3 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu1_deep.root","read")
    h0_i, h0_h = f0.Get("h1_init_energy"), f0.Get("h1_hit_energy")
    h1_i, h1_h = f1.Get("h1_init_energy"), f1.Get("h1_hit_energy")
    h2_i, h2_h = f2.Get("h1_init_energy"), f2.Get("h1_hit_energy")
    h3_i, h3_h = f3.Get("h1_init_energy"), f3.Get("h1_hit_energy")
    h0_c = h0_i.Clone(); h1_c = h1_i.Clone(); h2_c = h2_i.Clone(); h3_c = h3_i.Clone();
    h0_c.Add(h0_h,-1); h1_c.Add(h1_h,-1); h2_c.Add(h2_h,-1); h3_c.Add(h3_h,-1);
    h0_c.Divide(h0_i); h1_c.Divide(h1_i); h2_c.Divide(h2_i); h3_c.Divide(h3_i);
    h0_c.SetStats(0)
    h1_c.SetStats(0)
    h2_c.SetStats(0)
    h3_c.SetStats(0)
    h0_c.GetXaxis().SetRangeUser(10,180)
    h1_c.GetXaxis().SetRangeUser(10,180)
    h2_c.GetXaxis().SetRangeUser(10,180)
    h3_c.GetXaxis().SetRangeUser(10,180)

    h0_c.SetTitle("Self-Absorption;Energy [keV];Absorption Rate")
    h0_c.SetLineColor(1)
    h1_c.SetLineColor(2)
    h2_c.SetLineColor(ROOT.kTeal+2)
    h3_c.SetLineColor(4)

    leg = ROOT.TLegend(.6,.7,.95,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)
    leg.AddEntry(h0_c, "front new", "l")
    leg.AddEntry(h1_c, "back new", "l")
    leg.AddEntry(h2_c, "front old", "l")
    leg.AddEntry(h3_c, "back old", "l")

    cv=ROOT.TCanvas("cv","cv",1200,1200)
    cv.cd()
    ROOT.gPad.SetLogy(1)
    h0_c.Draw("hist")
    h1_c.Draw("hist same")
    h2_c.Draw("hist same")
    h3_c.Draw("hist same")
    leg.Draw("same")
    cv.Print("/Users/chiu.i-huan/Desktop/temp_output/Ryugu_absorption.pdf")

    cv2=ROOT.TCanvas("cv2","cv2",1200,1200)
    cv2.cd()
    ROOT.gPad.SetLogy(0)
    h0_c2, h1_c2, h2_c2, h3_c2 = h0_c.Clone(), h1_c.Clone(), h2_c.Clone(), h3_c.Clone()
    h0_c2.Rebin(8)
    h1_c2.Rebin(8)
    h2_c2.Rebin(8)
    h3_c2.Rebin(8)
    h0_c2.Divide(h2_c2)
    h1_c2.Divide(h3_c2)
    h0_c2.SetLineColor(2)
    h1_c2.SetLineColor(4)
    h0_c2.GetYaxis().SetRangeUser(0.6,1.6)
    h0_c2.GetXaxis().SetRangeUser(10,180)
    h0_c2.SetTitle("Self-Absorption;Energy [keV];Ratio")
    h0_c2.Draw("hist")
    h1_c2.Draw("hist same")
    leg2 = ROOT.TLegend(.2,.7,.45,.95)
    leg2.SetFillColorAlpha(0,0)
    leg2.SetLineColorAlpha(0,0)
    leg2.SetBorderSize(0)
    leg2.AddEntry(h0_c2, "new/old (front)", "l")
    leg2.AddEntry(h1_c2, "new/old (back)", "l")
    leg2.Draw("same")
    myline, myline2, myline3= ROOT.TLine(10,1,180,1), ROOT.TLine(10,1.1,180,1.1), ROOT.TLine(10,0.9,180,0.9)
    myline.SetLineColor(1)
    myline2.SetLineColor(1)
    myline3.SetLineColor(1)
    myline2.SetLineStyle(9)
    myline3.SetLineStyle(9)
    myline.Draw("same")
    myline2.Draw("same")
    myline3.Draw("same")
    cv2.Print("/Users/chiu.i-huan/Desktop/temp_output/Ryugu_absorptioni_ratio.pdf")

def mk_1d_absor():
    sample_name_list=["Ryugu","Murray","Yamato","Tagishi","Orguiel","Fe"] 
    paper_name_list=["(a)","(c)","(d)","(f)","(b)","None"]
    fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/absorption_1d_output_v2.root","recreate")
    ROOT.gStyle.SetOptFit(1)
    cv_fitname="/Users/chiu.i-huan/Desktop/temp_output/absorption_1d_fitting_all.pdf"
    cv = ROOT.TCanvas(cv_fitname,cv_fitname,0,0,1200,1000)
    cv.Print(cv_fitname + "[", "pdf")
    for sample_name in sample_name_list:
       f1 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/v2/Output_{}.root".format(sample_name),"read")
       f0 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/v2/Output_noSample_{}.root".format(sample_name),"read")
       h2_h_init, h2_h_sample = f0.Get("h2_hit_energy"),  f1.Get("h2_hit_energy")
       if sample_name == "Tagishi": sample_name = "Tagish"
       if sample_name == "Orguiel": sample_name = "Orgueil"

       leg = ROOT.TLegend(.6,.5,.9,.9)
       leg.SetFillColorAlpha(0,0)
       leg.SetLineColorAlpha(0,0)
       leg.SetBorderSize(0)
       cv=ROOT.TCanvas("cv_{}".format(sample_name),"cv_{}".format(sample_name),1200,1200)
       cv.cd()
       ROOT.gPad.SetLogy(0)
       hi_c = [ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D()]
       for i in range(6):
          hi = h2_h_init.ProjectionX("hi_{}".format(i),i+2,i+2)
          hs = h2_h_sample.ProjectionX("hs_{}".format(i),i+2,i+2)
          #hi.Rebin(10)
          #hs.Rebin(10)
          hi_c[i] = hi.Clone()
          hi_c[i].Add(hs,-1)
          hi_c[i].Divide(hi)
          #hi_c[i].SetStats(0)
          hi_c[i].GetXaxis().SetRangeUser(30,140)

          hi_c[i].SetTitle("Self-Absorption;Energy [keV];Absorption Rate")
          hi_c[i].GetXaxis().CenterTitle()
          hi_c[i].GetYaxis().CenterTitle()
          hi_c[i].SetLineColor(i+1)
          hi_c[i].SetMinimum(0)
          hi_c[i].SetMaximum(0.8)
          if sample_name == "Fe": hi_c[i].SetMaximum(1)
          if i == 0: hi_c[i].Draw("hist")
          else: hi_c[i].Draw("hist same")
          leg.AddEntry(hi_c[i], "CH{}".format(i+1), "l")
       leg.Draw("same")
       cv.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/Ryugu_absorption_6ch_{}_v2.pdf".format(sample_name))

       fout.cd()
       for i, _hist in enumerate(hi_c):
          # === fitting with pol5 === 
          f1=ROOT.TF1()
          if sample_name == "Tagish": f1=ROOT.TF1("f1_CH{0}_{1}".format(i+1,sample_name),"pol6",20,160)
          else: f1=ROOT.TF1("f1_CH{0}_{1}".format(i+1,sample_name),"pol5",20,160)
          f1.SetLineColor(2)
          _hist.Fit("f1_CH{0}_{1}".format(i+1,sample_name),"QR")
          #par_list = [f1.GetParameter(i) for i in range(6)]
          chi2=f1.GetChisquare()
          f1.Write()

          _hist.SetLineColor(1)
          _hist.SetName("hist_CH{0}_{1}".format(i+1,sample_name))
          _hist.Write()

          # === draw ===
          cv.cd()
          f1.SetLineColorAlpha(0,0)#for paper
          _hist.SetStats(0)# for paper
          _hist.Draw()
          latex = getLatex()
          #latex.DrawLatex(0.35,0.55,"{1}, CH{0}".format(i+1,sample_name))
          latex.DrawLatex(0.35,0.55,paper_name_list[i])
          cv.Print(cv_fitname,"pdf")
    cv.Print(cv_fitname + "]", "pdf")
    fout.Close()

def mk_eff():
   fint_new = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/xray_build/Output_ryugu_eff_new_4pi.root","read")
   fint_old = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/xray_build/Output_ryugu_eff_old_4pi.root","read")
   h2_init_energy_new = fint_new.Get("h2_init_energy")
   h2_hit_energy_new = fint_new.Get("h2_hit_energy")
   h2_init_energy_old = fint_old.Get("h2_init_energy")
   h2_hit_energy_old = fint_old.Get("h2_hit_energy")

   leg2 = ROOT.TLegend(.2,.7,.45,.95)
   leg2.SetFillColorAlpha(0,0)
   leg2.SetLineColorAlpha(0,0)
   leg2.SetBorderSize(0)
   for i in range(6):
      hori_n = h2_init_energy_new.ProjectionX("hori_n_{}".format(i),i+2,i+2)
      hhit_n = h2_hit_energy_new.ProjectionX("hhit_n_{}".format(i),i+2,i+2)
      hori_o = h2_init_energy_old.ProjectionX("hori_o_{}".format(i),i+2,i+2)
      hhit_o = h2_hit_energy_old.ProjectionX("hhit_o_{}".format(i),i+2,i+2)
      hhit_n.Divide(hori_n)
      hhit_o.Divide(hori_o)
      hhit_n.GetXaxis().SetRangeUser(10,180)
      hhit_o.GetXaxis().SetRangeUser(10,180)
      hhit_n.SetTitle(";Energy [keV]; Eff. [%]")
      hhit_n.GetXaxis().CenterTitle()
      hhit_n.GetYaxis().CenterTitle()
      hhit_n.Scale(100/2592.) # angle is 5/180 * 5/360 = 162
      hhit_o.Scale(100/2592.)
      hhit_n.SetStats(0)
      hhit_o.SetStats(0)
      hhit_n.SetLineColor(2)
      hhit_o.SetLineColor(4)
      hhit_n.SetMarkerColor(2)
      hhit_o.SetMarkerColor(4)
      hhit_n.SetMinimum(0)
      hhit_n.SetMaximum(0.05)

      cv=ROOT.TCanvas("cv{}".format(i),"cv{}".format(i),1000,1200)
      cv.cd()
      pad1,pad2 = ROOT.TPad("pad1_{}".format(i),"pad1_{}".format(i),0,0.3,1,1), ROOT.TPad("pad2_{}".format(i),"pad2_{}".format(i),0,0.03,1,0.3)
      pad1.SetBottomMargin(0.01)
      pad2.SetTopMargin(0)
      pad2.SetBottomMargin(0.4)
      pad2.Draw();pad1.Draw();

      pad1.cd()
      hhit_n.Draw("hist")
      hhit_o.Draw("hist same")
      if i == 0:
         leg2.AddEntry(hhit_n, "new", "l")
         leg2.AddEntry(hhit_o, "old", "l")
      leg2.Draw("same")
      latex = getLatex()
      latex.DrawLatex(0.65,0.25,"CH{}".format(i+1))

      pad2.cd()
      ho, h_residual = hhit_o.Clone(), hhit_n.Clone()
      h_residual.Rebin(20)
      ho.Rebin(20)
      h_residual.Divide(ho)
      h_residual.SetLineColor(1)
      h_residual.SetMarkerColor(1)
      h_residual.GetXaxis().SetRangeUser(10,180)
      h_residual.SetTitle(";Energy [keV];New/Old")
      h_residual.GetXaxis().SetTitleSize(35);
      h_residual.GetXaxis().SetTitleFont(43);
      h_residual.GetXaxis().SetTitleOffset(4.);
      h_residual.GetXaxis().SetLabelFont(43);
      h_residual.GetXaxis().SetLabelSize(30);
      h_residual.GetYaxis().SetNdivisions(505);
      h_residual.GetYaxis().SetTitleSize(25);
      h_residual.GetYaxis().SetTitleFont(43);
      h_residual.GetYaxis().SetTitleOffset(1.55);
      h_residual.GetYaxis().SetLabelFont(43);
      h_residual.GetYaxis().SetLabelSize(30);
      h_residual.SetMinimum(0.85)
      h_residual.SetMaximum(1.08)
      h_residual.SetMarkerSize(1)
      h_residual.Draw("hist")
      line = ROOT.TLine(10,1,180,1)
      line.SetLineColorAlpha(ROOT.kRed, 0.3)
      line.SetLineWidth(2)
      line.Draw("same")
      cv.Print("/Users/chiu.i-huan/Desktop/temp_output/Ryugu_Ge_eff_ch{}_v2.pdf".format(i+1))   

def mk_paper_plot():
    sample_name_list=["Ryugu","Murray","Yamato","Tagishi","Orguiel"] 
    paper_name_list=["(a)","(c)","(d)","(f)","(b)"]
    for j, sample_name in enumerate(sample_name_list):
       f1 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/v2/Output_{}.root".format(sample_name),"read")
       f0 = ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/absorption_sim/v2/Output_noSample_{}.root".format(sample_name),"read")
       h2_h_init, h2_h_sample = f0.Get("h2_hit_energy"),  f1.Get("h2_hit_energy")
       if sample_name == "Tagishi": sample_name = "Tagish"
       if sample_name == "Orguiel": sample_name = "Orgueil"

       hi_c = [ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D(),ROOT.TH1D()]
       for i in range(6):
          cv=ROOT.TCanvas("cv_{}_{}".format(sample_name,i),"cv_{}_{}".format(sample_name,i),1200,1000)
          cv.cd()
          ROOT.gPad.SetLogy(0)
          hi = h2_h_init.ProjectionX("hi_{}".format(i),i+2,i+2)
          hs = h2_h_sample.ProjectionX("hs_{}".format(i),i+2,i+2)
          hi_c[i] = hi.Clone()
          hi_c[i].Add(hs,-1)
          hi_c[i].Divide(hi)
          hi_c[i].SetStats(0)
          hi_c[i].GetXaxis().SetRangeUser(30,140)

          hi_c[i].SetTitle("Self-Absorption;Energy [keV];Absorption Rate")
          hi_c[i].GetXaxis().CenterTitle()
          hi_c[i].GetYaxis().CenterTitle()
          hi_c[i].SetLineColor(1)
          hi_c[i].SetMinimum(0)
          hi_c[i].SetMaximum(0.8)
          hi_c[i].Draw("hist")
          latex = getLatex()
          latex.DrawLatex(0.65,0.75,paper_name_list[j])
          cv.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/MaPS_RyuguPaper_fig2_{}_CH{}.pdf".format(sample_name,i+1))

    f_resp=ROOT.TFile("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/Output_Ryugu_resp2dim_0227_sum_5p6B_final.root","read")
    for i in range(6):
       h3=f_resp.Get("resp2dim_ch{}".format(i+1))
       cv=ROOT.TCanvas("cv2d_{}".format(i),"cv2d_{}".format(i),1200,1000)
       cv.cd()
       h3.SetTitle("CH3;Response [keV];Energy [keV]")
       h3.GetXaxis().CenterTitle()
       h3.GetYaxis().CenterTitle()
       ROOT.gPad.SetLogz(1)
       ROOT.gPad.SetRightMargin(0.15)
       ROOT.gStyle.SetPalette(ROOT.kInvertedDarkBodyRadiator)
       h3.Rebin2D(10,10)
       h3.GetXaxis().SetRangeUser(10,140)
       h3.GetYaxis().SetRangeUser(30,140)
       h3.Draw("colz")
       latex.DrawLatex(0.6,0.35,"(f)")
       cv.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/MaPS_RyuguPaper_fig2_Response_CH{}.pdf".format(i+1))

if __name__=="__main__":

#   mk_depth()
#   mk_absor()
#   mk_eff()
#   mk_1d_absor()
   mk_paper_plot()
