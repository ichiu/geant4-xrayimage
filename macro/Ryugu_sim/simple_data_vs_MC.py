import os,ROOT
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/202112013_ch2_152Eu_10cm_0pirad.root"#ch2
#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/20211130_ch3_152Eu_10cm_0pirad.root"#ch3
#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/202112013_ch2_152Eu_10cm_025pirad.root"
#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/202112013_ch2_152Eu_10cm_05pirad.root"
#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/20211130_ch3_152Eu_10cm_025pirad.root"
#data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/RIcsv_data/20211208/20211130_ch3_152Eu_10cm_05pirad.root"

data_file_blank="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203307_01_001_000_ene.root"#Blank
data_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE_"# MUSE203302:Co, MUSE203303:Eu, MUSE203298:Ba
tree_name="edbtree"
scale_blank_dic={"Co57":1833./46571, "Eu152":3947./46571, "Ba133":3628./46571} 
ndecay_data_dic={"Co57":1222778353, "Eu152":2716632871, "Ba133":15885366}

mc_file="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_RI_"
ndecay_mc=180_000_000
outname="Ba133"# Co57, Eu152, Ba133
addname="_revise"# Additional name
#addname=""# Additional name

nbins=int(8000/20)

def getLatex():
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def fit(_h):
    three_peaks=[40.17, 45.42, 121.78]
    count_list=[]
     
    return count_list
if __name__=="__main__":

  scale_blank=scale_blank_dic[outname]
  ndecay_data=ndecay_data_dic[outname]
  f_data_blank=ROOT.TFile(data_file_blank,"read")
  f_data=ROOT.TFile(data_file+outname+"_ene.root","read")
  f_mc=ROOT.TFile(mc_file+outname+addname+".root","read")

  t_data_blank=f_data_blank.Get(tree_name)
  t_data=f_data.Get(tree_name)
  t_mc=f_mc.Get("tree")
  t_data_blank.Draw("energy >> h_data_1_b({},0,200)".format(nbins),"ch==1","")
  t_data_blank.Draw("energy >> h_data_2_b({},0,200)".format(nbins),"ch==2","")
  t_data_blank.Draw("energy >> h_data_3_b({},0,200)".format(nbins),"ch==3","")
  t_data_blank.Draw("energy >> h_data_4_b({},0,200)".format(nbins),"ch==4","")
  t_data_blank.Draw("energy >> h_data_5_b({},0,200)".format(nbins),"ch==5","")
  t_data_blank.Draw("energy >> h_data_6_b({},0,200)".format(nbins),"ch==6","")
  t_data.Draw("energy >> h_data_1({},0,200)".format(nbins),"ch==1","")
  t_data.Draw("energy >> h_data_2({},0,200)".format(nbins),"ch==2","")
  t_data.Draw("energy >> h_data_3({},0,200)".format(nbins),"ch==3","")
  t_data.Draw("energy >> h_data_4({},0,200)".format(nbins),"ch==4","")
  t_data.Draw("energy >> h_data_5({},0,200)".format(nbins),"ch==5","")
  t_data.Draw("energy >> h_data_6({},0,200)".format(nbins),"ch==6","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_1({},0,200)".format(nbins),"Det_ID == 1","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_2({},0,200)".format(nbins),"Det_ID == 2","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_3({},0,200)".format(nbins),"Det_ID == 3","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_4({},0,200)".format(nbins),"Det_ID == 4","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_5({},0,200)".format(nbins),"Det_ID == 5","")
  t_mc.Draw("Hit_Energy_Reso*1000 >> h_mc_6({},0,200)".format(nbins),"Det_ID == 6","")
  h_data_1_b=ROOT.gDirectory.Get("h_data_1_b")
  h_data_2_b=ROOT.gDirectory.Get("h_data_2_b")
  h_data_3_b=ROOT.gDirectory.Get("h_data_3_b")
  h_data_4_b=ROOT.gDirectory.Get("h_data_4_b")
  h_data_5_b=ROOT.gDirectory.Get("h_data_5_b")
  h_data_6_b=ROOT.gDirectory.Get("h_data_6_b")
  h_data_1=ROOT.gDirectory.Get("h_data_1")
  h_data_2=ROOT.gDirectory.Get("h_data_2")
  h_data_3=ROOT.gDirectory.Get("h_data_3")
  h_data_4=ROOT.gDirectory.Get("h_data_4")
  h_data_5=ROOT.gDirectory.Get("h_data_5")
  h_data_6=ROOT.gDirectory.Get("h_data_6")
  h_mc_1=ROOT.gDirectory.Get("h_mc_1")
  h_mc_2=ROOT.gDirectory.Get("h_mc_2")
  h_mc_3=ROOT.gDirectory.Get("h_mc_3")
  h_mc_4=ROOT.gDirectory.Get("h_mc_4")
  h_mc_5=ROOT.gDirectory.Get("h_mc_5")
  h_mc_6=ROOT.gDirectory.Get("h_mc_6")

  h_data_1_b.Scale(scale_blank)
  h_data_2_b.Scale(scale_blank)
  h_data_3_b.Scale(scale_blank)
  h_data_4_b.Scale(scale_blank)
  h_data_5_b.Scale(scale_blank)
  h_data_6_b.Scale(scale_blank)
  h_data_1.Add(h_data_1_b,-1)
  h_data_2.Add(h_data_2_b,-1)
  h_data_3.Add(h_data_3_b,-1)
  h_data_4.Add(h_data_4_b,-1)
  h_data_5.Add(h_data_5_b,-1)
  h_data_6.Add(h_data_6_b,-1)

  h_data_1_b.SetLineColorAlpha(3,0.9)
  h_data_2_b.SetLineColorAlpha(3,0.9)
  h_data_3_b.SetLineColorAlpha(3,0.9)
  h_data_4_b.SetLineColorAlpha(3,0.9)
  h_data_5_b.SetLineColorAlpha(3,0.9)
  h_data_6_b.SetLineColorAlpha(3,0.9)
  h_data_1.SetLineColorAlpha(2,0.9)
  h_data_2.SetLineColorAlpha(2,0.9)
  h_data_3.SetLineColorAlpha(2,0.9)
  h_data_4.SetLineColorAlpha(2,0.9)
  h_data_5.SetLineColorAlpha(2,0.9)
  h_data_6.SetLineColorAlpha(2,0.9)
  h_mc_1.SetLineColorAlpha(4,0.9)
  h_mc_2.SetLineColorAlpha(4,0.9)
  h_mc_3.SetLineColorAlpha(4,0.9)
  h_mc_4.SetLineColorAlpha(4,0.9)
  h_mc_5.SetLineColorAlpha(4,0.9)
  h_mc_6.SetLineColorAlpha(4,0.9)
  h_data_1_b.SetLineWidth(1)
  h_data_2_b.SetLineWidth(1)
  h_data_3_b.SetLineWidth(1)
  h_data_4_b.SetLineWidth(1)
  h_data_5_b.SetLineWidth(1)
  h_data_6_b.SetLineWidth(1)
  h_data_1.SetLineWidth(1)
  h_data_2.SetLineWidth(1)
  h_data_3.SetLineWidth(1)
  h_data_4.SetLineWidth(1)
  h_data_5.SetLineWidth(1)
  h_data_6.SetLineWidth(1)
  h_mc_1.SetLineWidth(1)
  h_mc_2.SetLineWidth(1)
  h_mc_3.SetLineWidth(1)
  h_mc_4.SetLineWidth(1)
  h_mc_5.SetLineWidth(1)
  h_mc_6.SetLineWidth(1)
  h_mc_1.Scale(ndecay_data/ndecay_mc)
  h_mc_2.Scale(ndecay_data/ndecay_mc)
  h_mc_3.Scale(ndecay_data/ndecay_mc)
  h_mc_4.Scale(ndecay_data/ndecay_mc)
  h_mc_5.Scale(ndecay_data/ndecay_mc)
  h_mc_6.Scale(ndecay_data/ndecay_mc)

  leg = ROOT.TLegend(.75,.75,.9,.9)
  leg.SetFillColor(0)
  leg.SetLineColor(0)
  leg.SetBorderSize(0)
  leg.AddEntry(h_data_1_b, "blank" , "l")
  leg.AddEntry(h_data_1, "data" , "l")
  leg.AddEntry(h_mc_1, "mc" , "l")

  latex = getLatex()
  c=ROOT.TCanvas("c","c",1200,800)
  c.Divide(3,2)
  c.cd(1)
  ROOT.gPad.SetLogy(1)
  h_data_1.Draw("hist")
  h_mc_1.Draw("hist same")
  h_data_1_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH1")
  leg.Draw("same")
  c.cd(2)
  ROOT.gPad.SetLogy(1)
  h_data_2.Draw("hist")
  h_mc_2.Draw("hist same")
  h_data_2_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH2")
  c.cd(3)
  ROOT.gPad.SetLogy(1)
  h_data_3.Draw("hist")
  h_mc_3.Draw("hist same")
  h_data_3_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH3")
  c.cd(4)
  ROOT.gPad.SetLogy(1)
  h_data_4.Draw("hist")
  h_mc_4.Draw("hist same")
  h_data_4_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH4")
  c.cd(5)
  ROOT.gPad.SetLogy(1)
  h_data_5.Draw("hist")
  h_mc_5.Draw("hist same")
  h_data_5_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH5")
  c.cd(6)
  ROOT.gPad.SetLogy(1)
  h_data_6.Draw("hist")
  h_mc_6.Draw("hist same")
  h_data_6_b.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH6")

  c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_data_vs_mc_{}.png".format(outname))

  c2=ROOT.TCanvas("c2","c2",1200,800)
  c2.Divide(3,2)
  c2.cd(1)
  ROOT.gPad.SetLogy(0)
  h_data_1.Draw("hist")
  h_mc_1.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH1")
  leg.Draw("same")
  c2.cd(2)
  ROOT.gPad.SetLogy(0)
  h_data_2.Draw("hist")
  h_mc_2.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH2")
  c2.cd(3)
  ROOT.gPad.SetLogy(0)
  h_data_3.Draw("hist")
  h_mc_3.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH3")
  c2.cd(4)
  ROOT.gPad.SetLogy(0)
  h_data_4.Draw("hist")
  h_mc_4.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH4")
  c2.cd(5)
  ROOT.gPad.SetLogy(0)
  h_data_5.Draw("hist")
  h_mc_5.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH5")
  c2.cd(6)
  ROOT.gPad.SetLogy(0)
  h_data_6.Draw("hist")
  h_mc_6.Draw("hist same")
  latex.DrawLatex(0.5,0.85,"CH6")

  c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_data_vs_mc_linear_{}.png".format(outname))
