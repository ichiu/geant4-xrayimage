"""
simple_fit.py used to find the counts of the peaks by fitting
bkg : pol1
signal : single gaus
"""

import os,ROOT,ctypes,argparse
from ROOT import gROOT, gStyle
gROOT.SetBatch(1)

ROOT.gErrorIgnoreLevel = ROOT.kFatal
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()

# This is important (same with mk_eff_MCvsData.py)
nbins=int(8000)

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def fit6ch(args):
    """
    Ryugu data
    None : MUSE203307_01_001_000_ene.root
    Co57 : MUSE203302_01_001_000_ene.root
    Eu152 : MUSE203303_01_001_000_ene.root
    Ba133 : MUSE203305_01_001_000_ene.root
    """
    # === tree ===
    ROOT.gROOT.ProcessLine(
      "struct RootStruct {\
         Int_t      ch;\
         Double_t  peak;\
         Int_t  peakID;\
         Double_t  intensity;\
         Double_t  eff;\
      };"
    );
    from ROOT import RootStruct
    struct = RootStruct()
    tout=ROOT.TTree('tree','tree')
    tout.Branch( 'ch', ROOT.addressof( struct, 'ch' ),  'ch/I' )
    tout.Branch( 'peak', ROOT.addressof( struct, 'peak' ),  'peak/D' )
    tout.Branch( 'peakID', ROOT.addressof( struct, 'peakID' ),  'peakID/I' )
    tout.Branch( 'intensity', ROOT.addressof( struct, 'intensity' ),  'intensity/D' )
    tout.Branch( 'eff', ROOT.addressof( struct, 'eff' ),  'eff/D' )

    # === selectable regions ===
    if "co" in args.output.lower():
       overall_range_down=[13.5,121,135.5]
       overall_range_up=[15.1,122.8,137.5]
       emissivity=[9.16, 85.60, 10.68] #%
       cout_all=1222778353 # source in Ryugu Exp.
    elif "euadd" in args.output.lower():
       overall_range_down=[38.5]
       overall_range_up=[41.5]
       emissivity=[21.1, 38.3] #%
       cout_all=2716632871
    elif "eu" in args.output.lower():
       #overall_range_down=[38.5, 40, 44.5,46.3,120]
       #overall_range_up=[39.8, 41.5,46,48,123]
       #emissivity=[21.1, 38.3, 10.98, 3.3, 28.58] #%
       overall_range_down=[44.5, 120]
       overall_range_up=[46, 123]
       emissivity=[10.98, 28.58] #%
       cout_all=2716632871
    elif "baadd" in args.output.lower():
       overall_range_down=[30]
       overall_range_up=[32]
       emissivity=[34.9, 64.5] #%
       cout_all=15885366
    elif "ba" in args.output.lower():
       overall_range_down=[34.0, 80.2]
       overall_range_up=[35.4, 81.9]
       emissivity=[5.99+11.6, 34.06] #%
       cout_all=15885366
       #cout_all=5206906#Note
       #cout_all=23476500 #203298
       #cout_all=180729923 #203297
    else:
       print("check output name!")
       return [],[]
    if "mc" in args.output.lower(): cout_all=180000000

    f_int=ROOT.TFile(args.input,"read")
    t_int=f_int.Get(args.tree)

    peaks_list,count_list=[],[]
    for ich in range(1,7):
       print("=====================================")
       print("CH{} : ".format(ich))
       #t_int.Draw("energy >> h({},0,200)".format(nbins),"ch=={}".format(ich),"")#//for data
       t_int.Draw("Hit_Energy_Reso*1000 >> h({},0,200)".format(nbins),"Det_ID=={}".format(ich),"")#//for MC
       _h=ROOT.gDirectory.Get("h")
       for _ip in range(len(overall_range_down)):
          _htemp,_hdraw=_h.Clone(), _h.Clone()
          _htemp.GetXaxis().SetRangeUser(overall_range_down[_ip],overall_range_up[_ip])
          _hdraw.GetXaxis().SetRangeUser(overall_range_down[_ip],overall_range_up[_ip])
   
          if args.output.lower() == "euadd":
             #peak = ROOT.TF1("peak","gaus", 38.5, 39.8)
             #peak2 = ROOT.TF1("peak2","gaus", 39.8, 41.5)
             peak = ROOT.TF1("peak","gaus", overall_range_down[_ip], 40.5)
             peak2 = ROOT.TF1("peak2","gaus", 39.5, overall_range_up[_ip])
             bkg = ROOT.TF1("bkg","pol1", overall_range_down[_ip],overall_range_up[_ip])

             par0,par1,par2,par3,par4,par5,par6,par7 = map(ctypes.c_double, (0,0,0,0,0,0,0,0))
             _htemp.Fit(peak,"QR")
             _htemp.Fit(peak2,"QR+")
             _htemp.Fit(bkg,"QR+")
             par0=peak.GetParameter(0)
             par1=peak.GetParameter(1)
             par2=peak.GetParameter(2)
             par3=peak2.GetParameter(0)
             par4=peak2.GetParameter(1)
             par5=peak2.GetParameter(2)
             par6=bkg.GetParameter(0)
             par7=bkg.GetParameter(1)

             total = ROOT.TF1("total","gaus(0)+gaus(3)+pol1(6)", overall_range_down[_ip],overall_range_up[_ip] )
             total.SetParameters(par0,par1,par2,par3,par4,par5,par6,par7)
             _htemp.Fit(total,"QR+")
             par0,par1,par2,par3,par4,par5,par6,par7=total.GetParameter(0),total.GetParameter(1),total.GetParameter(2),total.GetParameter(3),total.GetParameter(4),total.GetParameter(5),total.GetParameter(6),total.GetParameter(7)
             peak.SetParameters(par0,par1,par2)
             peak2.SetParameters(par3,par4,par5)
             bkg.SetParameters(par6,par7)
             _binwidth=_htemp.GetBinWidth(1)
             c1=ROOT.TCanvas("c1{}".format(_ip),"c1{}".format(_ip),1200,800) 
             _hdraw.SetLineColor(1)
             peak.SetLineColor(4)
             peak2.SetLineColor(4)
             bkg.SetLineColor(3)
             total.SetLineColor(2)
             _hdraw.SetMinimum(0)
             _hdraw.Draw("ep")
             peak.Draw("same")
             peak2.Draw("same")
             total.Draw("same")
             bkg.Draw("same")
             c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_{0}_peak{1}_CH{2}.png".format(args.output,_ip,ich))
             # === for tree ===
             struct.ch = ich
             struct.peak = par1
             struct.intensity = peak.Integral(par1-3*par2,par1+3*par2)/_binwidth
             struct.eff = struct.intensity/(cout_all*emissivity[0]/100.)
             tout.Fill()
             struct.ch = ich
             struct.peak = par4
             struct.intensity = peak2.Integral(par4-3*par5,par4+3*par5)/_binwidth
             struct.eff = struct.intensity/(cout_all*emissivity[1]/100.)
             tout.Fill()
          elif args.output.lower() == "baadd":
             peak = ROOT.TF1("peak","gaus", overall_range_down[_ip], 31.2)
             peak2 = ROOT.TF1("peak2","gaus", 30.5, overall_range_up[_ip])
             bkg = ROOT.TF1("bkg","pol1", overall_range_down[_ip],overall_range_up[_ip])

             par0,par1,par2,par3,par4,par5,par6,par7 = map(ctypes.c_double, (0,0,0,0,0,0,0,0))
             _htemp.Fit(peak,"QR")
             _htemp.Fit(peak2,"QR+")
             _htemp.Fit(bkg,"QR+")
             par0=peak.GetParameter(0)
             par1=peak.GetParameter(1)
             par2=peak.GetParameter(2)
             par3=peak2.GetParameter(0)
             par4=peak2.GetParameter(1)
             par5=peak2.GetParameter(2)
             par6=bkg.GetParameter(0)
             par7=bkg.GetParameter(1)

             total = ROOT.TF1("total","gaus(0)+gaus(3)+pol1(6)", overall_range_down[_ip],overall_range_up[_ip] )
             total.SetParameters(par0,par1,par2,par3,par4,par5,par6,par7)
             _htemp.Fit(total,"QR+")
             par0,par1,par2,par3,par4,par5,par6,par7=total.GetParameter(0),total.GetParameter(1),total.GetParameter(2),total.GetParameter(3),total.GetParameter(4),total.GetParameter(5),total.GetParameter(6),total.GetParameter(7)
             peak.SetParameters(par0,par1,par2)
             peak2.SetParameters(par3,par4,par5)
             bkg.SetParameters(par6,par7)
             _binwidth=_htemp.GetBinWidth(1)
             c1=ROOT.TCanvas("c1{}".format(_ip),"c1{}".format(_ip),1200,800) 
             _hdraw.SetLineColor(1)
             peak.SetLineColor(4)
             peak2.SetLineColor(4)
             bkg.SetLineColor(3)
             total.SetLineColor(2)
             _hdraw.SetMinimum(0)
             _hdraw.Draw("ep")
             peak.Draw("same")
             peak2.Draw("same")
             total.Draw("same")
             bkg.Draw("same")
             c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_{0}_peak{1}_CH{2}.png".format(args.output,_ip,ich))
             # === for tree ===
             struct.ch = ich
             struct.peak = par1
             struct.intensity = peak.Integral(par1-3*par2,par1+3*par2)/_binwidth
             struct.eff = struct.intensity/(cout_all*emissivity[0]/100.)
             tout.Fill()
             struct.ch = ich
             struct.peak = par4
             struct.intensity = peak2.Integral(par4-3*par5,par4+3*par5)/_binwidth
             struct.eff = struct.intensity/(cout_all*emissivity[1]/100.)
             tout.Fill()
          else:
             peak = ROOT.TF1("peak","gaus", overall_range_down[_ip], overall_range_up[_ip])
             bkg = ROOT.TF1("bkg","pol1", overall_range_down[_ip],overall_range_up[_ip])

             par0,par1,par2,par3,par4 = map(ctypes.c_double, (0,0,0,0,0))
             _htemp.Fit(peak,"Q")
             _htemp.Fit(bkg,"Q")
             par0=peak.GetParameter(0)
             par1=peak.GetParameter(1)
             par2=peak.GetParameter(2)
             par3=bkg.GetParameter(0)
             par4=bkg.GetParameter(1)

             total = ROOT.TF1("total","gaus(0)+pol1(3)", overall_range_down[_ip],overall_range_up[_ip] )
             total.SetParameters(par0,par1,par2,par3,par4)
             _htemp.Fit(total,"Q")
             par0,par1,par2,par3,par4=total.GetParameter(0),total.GetParameter(1),total.GetParameter(2),total.GetParameter(3),total.GetParameter(4)
             peak.SetParameters(par0,par1,par2)
             bkg.SetParameters(par3,par4)
             
             peaks_list.append(par1)#peak
             _bindown, _binup=_htemp.GetXaxis().FindBin(par1-3*par2), _htemp.GetXaxis().FindBin(par1+3*par2)
             _binwidth=_htemp.GetBinWidth(1)
             count_list.append(peak.Integral(par1-3*par2,par1+3*par2)/_binwidth)
             print("Peak : {0:.1f} | Hist. : {1:.2f} | Total : {2:.2f} | Signal : {3:.2f}".format(par1, _htemp.Integral(_bindown,_binup), total.Integral(par1-3*par2,par1+3*par2)/_binwidth, peak.Integral(par1-3*par2,par1+3*par2)/_binwidth))
   
             c=ROOT.TCanvas("c{}".format(_ip),"c{}".format(_ip),1200,800)
             _hdraw.SetLineColor(1)
             peak.SetLineColor(4)
             bkg.SetLineColor(3)
             total.SetLineColor(2)
             _hdraw.SetMinimum(0)
             _hdraw.Draw("ep")
             peak.Draw("same")
             total.Draw("same")
             bkg.Draw("same")
             c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_{0}_peak{1}_CH{2}.png".format(args.output,_ip,ich))

             # === for tree ===
             struct.ch = ich
             struct.peak = par1
             struct.peakID=_ip
             struct.intensity = peak.Integral(par1-3*par2,par1+3*par2)/_binwidth
             struct.eff = struct.intensity/(cout_all*emissivity[_ip]/100.)
             tout.Fill()
    fout=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_result_{}.root".format(args.output),"recreate")
    fout.cd()
    tout.Write()
    return peaks_list, count_list


def fitRI(args):
    # === input ===
    f_list = []#Eu (12150.5), Ba(7008.7)
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/202111011_ch3_Eu152_10cm_0rad.root")#3600
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/202111013_ch3_Ba133_10cm_0pirad.root")#14536
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/20211208/20211130_ch3_152Eu_10cm_0pirad.root")#72000
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/20211107_ch2_Eu152_10cm_0rad.root")#3600
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/20211108_ch2_Ba133_10cm_0rad.root")#61208
    f_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/csv_analysis/root/20211208/202112013_ch2_152Eu_10cm_0pirad.root")#72000
    cout_all_list=[43741800,1.0187846e+08, 8.7483600e+08, 43741800, 4.2898851e+08, 8.7483600e+08]
    # === tree ===
    ROOT.gROOT.ProcessLine(
      "struct RootStruct {\
         Int_t      ch;\
         Double_t  peak;\
         Double_t  intensity;\
         Double_t  eff;\
      };"
    );
    from ROOT import RootStruct
    struct = RootStruct()
    tout=ROOT.TTree('tree','tree')
    tout.Branch( 'ch', ROOT.addressof( struct, 'ch' ),  'ch/I' )
    tout.Branch( 'peak', ROOT.addressof( struct, 'peak' ),  'peak/D' )
    tout.Branch( 'intensity', ROOT.addressof( struct, 'intensity' ),  'intensity/D' )
    tout.Branch( 'eff', ROOT.addressof( struct, 'eff' ),  'eff/D' )

    # === selectable regions ===
    for i in range(len(f_list)):
       cout_all=cout_all_list[i]
       if i==0 or i==2 or i==3 or i == 5:#Eu152
          overall_range_down=[38, 40, 44.5,46.3,120]
          overall_range_up=[39.7, 41.5,46,48,123]
          emissivity=[21.1, 38.3, 10.98, 3.3, 28.58] #%
       elif i == 1 or i ==4:#Ba133
          overall_range_down=[34.0, 52, 80.2]
          overall_range_up=[35.4, 55, 81.9]
          emissivity=[5.99+11.6, 2.199, 34.06] #%
       else:
          print("check output name!")
          return [],[]

       f_int=ROOT.TFile(f_list[i],"read")
       t_int=f_int.Get("tree")

       peaks_list,count_list=[],[]
       t_int.Draw("energy >> h({},0,200)".format(nbins),"","")
       _h=ROOT.gDirectory.Get("h")
       for _ip in range(len(overall_range_down)):
          _htemp,_hdraw=_h.Clone(), _h.Clone()
          _htemp.GetXaxis().SetRangeUser(overall_range_down[_ip],overall_range_up[_ip])
          _hdraw.GetXaxis().SetRangeUser(overall_range_down[_ip],overall_range_up[_ip])
          peak = ROOT.TF1("peak","gaus", overall_range_down[_ip], overall_range_up[_ip]);
          bkg = ROOT.TF1("bkg","pol1", overall_range_down[_ip],overall_range_up[_ip]);

          par0,par1,par2,par3,par4 = map(ctypes.c_double, (0,0,0,0,0))
          _htemp.Fit(peak,"Q")
          _htemp.Fit(bkg,"Q")
          par0=peak.GetParameter(0)
          par1=peak.GetParameter(1)
          par2=peak.GetParameter(2)
          par3=bkg.GetParameter(0)
          par4=bkg.GetParameter(1)
   
          total = ROOT.TF1("total","gaus(0)+pol1(3)", overall_range_down[_ip],overall_range_up[_ip] )
          total.SetParameters(par0,par1,par2,par3,par4)
          _htemp.Fit(total,"Q")
          par0,par1,par2,par3,par4=total.GetParameter(0),total.GetParameter(1),total.GetParameter(2),total.GetParameter(3),total.GetParameter(4)
          peak.SetParameters(par0,par1,par2)
          bkg.SetParameters(par3,par4)
          
          peaks_list.append(par1)#peak
          _bindown, _binup=_htemp.GetXaxis().FindBin(par1-3*par2), _htemp.GetXaxis().FindBin(par1+3*par2)
          _binwidth=_htemp.GetBinWidth(1)
          count_list.append(peak.Integral(par1-3*par2,par1+3*par2)/_binwidth)
          print("Peak : {0:.1f} | Hist. : {1:.2f} | Total : {2:.2f} | Signal : {3:.2f}".format(par1, _htemp.Integral(_bindown,_binup), total.Integral(par1-3*par2,par1+3*par2)/_binwidth, peak.Integral(par1-3*par2,par1+3*par2)/_binwidth))
   
          c=ROOT.TCanvas("c{}".format(_ip),"c{}".format(_ip),1200,800)
          _hdraw.SetLineColor(1)
          peak.SetLineColor(4)
          bkg.SetLineColor(3)
          total.SetLineColor(2)
          _hdraw.SetMinimum(0)
          _hdraw.Draw("ep")
          peak.Draw("same")
          total.Draw("same")
          bkg.Draw("same")
          c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_file{0}_peak{1}.png".format(i,_ip))

          # === for tree ===
          if i <=2: struct.ch = 3
          else: struct.ch = 2
          struct.peak = par1
          struct.intensity = peak.Integral(par1-3*par2,par1+3*par2)/_binwidth
          struct.eff = struct.intensity/(cout_all*emissivity[_ip]/100.)
          tout.Fill()
    fout=ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_fit_result_RI.root","recreate")
    fout.cd()
    tout.Write()

if __name__=="__main__":
  parser = argparse.ArgumentParser(description='Processing') 
  parser.add_argument("-i","--input", type=str, default="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203303_01_001_000_ene.root", help="Input File Name")
  parser.add_argument("-o","--output", type=str, default="Eu", help="Output Name")
  parser.add_argument("-t","--tree", type=str, default="edbtree", help="Input Tree Name")
  args = parser.parse_args()

  if "RI" not in args.output.lower(): _peak, _cout = fit6ch( args )# for ryugu study
  if args.output == "RI" : fitRI( args )# for other study

