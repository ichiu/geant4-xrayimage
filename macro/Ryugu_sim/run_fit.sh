python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203302_01_001_000_ene.root" -o "Co" 
python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203303_01_001_000_ene.root" -o "Eu" 
python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203303_01_001_000_ene.root" -o "EuAdd" 
#python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203305_01_001_000_ene.root" -o "Ba"
#python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Ryugu_sim/edb_data/MUSE203305_01_001_000_ene.root" -o "BaAdd"
#python simple_fit.py -i "/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/edb_analysis/root/MUSE203298_01_001_000_ene.root" -o "Ba"
#python simple_fit.py -i "/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/edb_analysis/root/MUSE203297_01_001_000_ene.root" -o "Ba"
#python simple_fit.py -i "NONE" -o "RI"
rm /Users/chiu.i-huan/Desktop/temp_output/output_fit_sum.root
hadd /Users/chiu.i-huan/Desktop/temp_output/output_fit_sum.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_Co.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_Eu.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_EuAdd.root  

python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_RI_Co57_eff.root" -o "Co_MC" -t "tree" 
python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_RI_Eu152_eff.root" -o "Eu_MC" -t "tree"
python simple_fit.py -i "/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/ryugu_build/Output_Ryugu_RI_Ba133_eff.root" -o "Ba_MC" -t "tree"
rm /Users/chiu.i-huan/Desktop/temp_output/output_fit_sum_MC.root
hadd /Users/chiu.i-huan/Desktop/temp_output/output_fit_sum_MC.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_Co_MC.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_Eu_MC.root /Users/chiu.i-huan/Desktop/temp_output/output_fit_result_Ba_MC.root
