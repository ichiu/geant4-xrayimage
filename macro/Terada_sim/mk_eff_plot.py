import os,ROOT
from array import array
from ROOT import *
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

if __name__=="__main__":
   fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/output_eff_plots.root","recreate")
   _path="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Terada_sim/G4_MCsample/0426/"
   sample_list=["Output_white.root", "Output_black.root", "Output_dew.root", "Output_dewbar.root", "Output_dewbar35.root"]
   _name=["eff_white","eff_black","eff_dew","eff_dewbar","eff_dewbar35"]

   for isample in range(len(sample_list)):
      f1=ROOT.TFile(_path+sample_list[isample],"read")
      tree=f1.Get("tree")
      h_base=f1.Get("h1_init_energy")
      h_base_2D=f1.Get("h2_init_energy")# for 6ch directions

      for i in range(6):
          _hbase_temp=h_base_2D.ProjectionX("px_{}".format(i+1),i+1,i+1)
          tree.Draw("Hit_Energy_Reso*1000 >> h1_{}_{}(8000,0,200)".format(_name[isample],i+1),"Det_ID=={}".format(i+2),"")
          h1=ROOT.gDirectory.Get("h1_{}_{}".format(_name[isample],i+1))
          h1.Divide(_hbase_temp)
      
          h1.SetTitle(";Energy [keV]; Efficiency")
          h1.GetXaxis().CenterTitle(); h1.GetYaxis().CenterTitle();
          h1.SetLineColorAlpha(1,0.9)
          h1.SetMarkerColor(1)
          h1.SetMarkerStyle(2)
          fout.cd()
          h1.Write()
      
          c2=ROOT.TCanvas("c2_{}_{}".format(_name[isample],i),"c2_{}_{}".format(_name[isample],i),1200,1200)
          c2.cd()
          ROOT.gPad.SetLogy(0)
          h1.Rebin(20)
          h1.Draw("hist")
      
          c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_terada_eff_{}_{}.pdf".format(_name[isample],i+1))

