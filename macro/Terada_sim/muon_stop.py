import sys, os, ROOT
from array import array
import numpy as np
ROOT.gErrorIgnoreLevel = ROOT.kFatal
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

momentum_range=[15,60]
sample_range=15
def plot_pos(_data):
    gr,gr_sigma=ROOT.TGraph(), ROOT.TGraph()
    fint = ROOT.TFile(_data,"read")
    tint = fint.Get("tree")
    results=[]
    for _m in range(momentum_range[0],momentum_range[1]+1):
       tint.Draw("muSampleEndPolZ >> h1({0},0,{1})".format(sample_range*1000,sample_range),"muInitpZ == {0}".format(_m),"")
       h1=ROOT.gDirectory.Get("h1")
       sumw,entries,cut_lowbin=0,0,5
       for i in range(1, sample_range*1000+1):
          if h1.GetBinContent(i) < cut_lowbin: continue
          entries+=h1.GetBinContent(i)
          sumw+=h1.GetBinCenter(i)*h1.GetBinContent(i)
       if entries == 0: entries = 1
       results.append([sumw/entries, h1.GetStdDev()]) 
       #results.append([h1.GetMean(), h1.GetStdDev()])
       print("{0:.3f}, {1:.3f}, {2:.3f}".format(h1.GetMean(),sumw/entries,h1.GetStdDev()))# slow

    for ir, result in enumerate(results):
       gr.SetPoint(ir,momentum_range[0]+ir,result[0])
       gr_sigma.SetPoint(ir, momentum_range[0]+ir, result[0]+result[1])

    results.reverse()
    for ir, result in enumerate(results):
       gr_sigma.SetPoint(len(results)+ir, momentum_range[1]-ir, result[0]-result[1])
       if ir == len(results)-1: gr_sigma.SetPoint(len(results)+ir+1, momentum_range[1]-ir, result[0]+result[1])
    gr.SetTitle(";Muon momentum [MeV/c]; Muon stopping depth [mm]")
    gr.GetXaxis().CenterTitle(); gr.GetYaxis().CenterTitle();
    gr.SetMinimum()
    return [gr,gr_sigma] 

def osawa_ryugu():
    y=np.array([0.10,0.12,0.15,0.19,0.22,0.27,0.32,0.38,0.44,0.51,0.59,0.68,0.77,0.88,1.00,1.12,1.26,1.41,1.57,1.74,1.92,2.12,2.34,2.56,2.80,3.06,3.33,3.62,3.92,4.24,4.58])
    x=np.array(range(15,46),dtype=float)
    gr,gr_sigma=ROOT.TGraph(len(x),x,y), ROOT.TGraph()
    return [gr,gr_sigma]

def osawa_NWA482():
    y=np.array([0.06,0.07,0.09,0.11,0.13,0.16,0.19,0.22,0.26,0.30,0.35,0.40,0.45,0.52,0.58,0.66,0.74,0.82,0.92,1.02,1.13,1.24,1.37,1.50,1.64,1.79,1.95,2.12,2.30,2.48,2.68])
    x=np.array(range(15,46),dtype=float)
    gr,gr_sigma=ROOT.TGraph(len(x),x,y), ROOT.TGraph()
    return [gr,gr_sigma]

def mk_cv(_name_list,_gr_list,_color_list):
    c=ROOT.TCanvas("c{}".format(1),"c{}".format(1),1200,1000)
    c.cd()
    #c.SetLogy(1)
    leg = ROOT.TLegend(.20,.35,.47,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    gr_ori=ROOT.TGraphAsymmErrors()
    gr_ori.SetPoint(0,0,0)
    gr_ori.SetMinimum(0);gr_ori.SetMaximum(sample_range)
    gr_ori.Draw()
    for i, (_name, _gr, _color) in enumerate(zip(_name_list,_gr_list,_color_list)):  
       _gr[0].SetLineColor(_color)
       _gr[0].SetMarkerColorAlpha(1,0)
       _gr[0].SetMarkerStyle(0)
       _gr[1].SetLineColorAlpha(0,0)
       _gr[1].SetFillColorAlpha(5,0.3)
       if i == 0: 
          _gr[0].SetMinimum(0);_gr[0].SetMaximum(sample_range)
          _gr[0].Draw()
          #leg.AddEntry(_gr[1],"1-#sigma","F") 
       else: _gr[0].Draw("SAME")
       #_gr[1].Draw("F SAME")
       leg.AddEntry(_gr[0],"{}".format(_name),"l") 
    leg.Draw("same")

    c.Print("/Users/chiu.i-huan/Desktop/temp_output/cv_meteorite.pdf")
    fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/tarada_mc_stopmuon.root","recreate")
    fout.cd()
    for i,(_g,_n) in enumerate(zip(_gr_list,_name_list)):
       _g[0].SetName(_n)
       _g[1].SetName(_n+"_sigma")
       _g[0].Write()
       _g[1].Write()
    print("figure : /Users/chiu.i-huan/Desktop/temp_output/cv_meteorite.pdf")
    print("output : /Users/chiu.i-huan/Desktop/temp_output/tarada_mc_stopmuon.root")

if __name__=="__main__":

    name_list,gr_list,color_list=[],[],[]
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_CI.root")); name_list.append("CI"); color_list.append(ROOT.kTeal+4)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_CM.root")); name_list.append("CM"); color_list.append(11)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_CV.root")); name_list.append("CV"); color_list.append(ROOT.kViolet-1)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_CO.root")); name_list.append("CO"); color_list.append(5)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_H4.root")); name_list.append("H4"); color_list.append(6)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_LL.root")); name_list.append("LL"); color_list.append(3)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_EH.root")); name_list.append("EH"); color_list.append(7)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_Ryugu.root")); name_list.append("Ryugu"); color_list.append(ROOT.kOrange)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_NWA032.root")); name_list.append("NWA032(black)"); color_list.append(2)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_NWA482.root")); name_list.append("NWA482(white)"); color_list.append(4)
    gr_list.append(plot_pos("/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_DEW12007_ori.root")); name_list.append("DEW12007"); color_list.append(1)
#    gr_list.append(osawa_ryugu()); name_list.append("Osawa ryugu"); color_list.append()
#    gr_list.append(osawa_NWA482());name_list.append("Osawa NWA482"); color_list.append()

    mk_cv(name_list, gr_list, color_list)
   
