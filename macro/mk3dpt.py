"""
This module makes a 3D plot
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2022-11-07"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"


import sys,os,random,math,time,ROOT,argparse, ctypes
from ROOT import TFile, TTree, gROOT, TCut, gDirectory, TMinuit, TMath, addressof
from root_numpy import hist2array, array2hist, tree2array
import numpy as np
from array import array
from scipy import ndimage, misc

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetFrameFillColor(0)
ROOT.gStyle.SetCanvasColor(0)
ROOT.gStyle.SetPadBorderSize(0)

from vedo import *
from vedo.applications import IsosurfaceBrowser
from vedo.applications import SlicerPlotter

def mkh3(args):
    f = ROOT.TFile(args.inputFolder, "read")
    t = f.Get("tree")
    t.Draw("muSampleEndPolZ:muSampleEndPolY:muSampleEndPolX >> h3(400,-10,10,400,-10,10,400,-1,1)")
    h3=gDirectory.Get("h3")
    h3.SetDirectory(0)
    fout = ROOT.TFile("MC_stop_h3.root","recreate")
    fout.cd()
    h3.Write()
    fout.Close()    

    #point_object=np.zeros((16000,16000,11000),dtype=float)
    #h3_d = ROOT.TH3D("h3_d","h3_d",2000,-10,10,2000,-10,10,2000,-1,1)
    #for i in range(t.GetEntries()):
    #   t.GetEntry(i)
    #   if t.muSampleEndPolX > 10 or t.muSampleEndPolX < -10: continue #-80 ~ 80
    #   if t.muSampleEndPolY > 10 or t.muSampleEndPolY < -10: continue #-80 ~ 80
    #   if t.muSampleEndPolZ > 1 or t.muSampleEndPolZ < -1: continue #-10 ~ 1
    #   h3_d.Fill(t.muSampleEndPolX,t.muSampleEndPolY,t.muSampleEndPolZ)
    #h3_d.Write()

def main():
    f = ROOT.TFile("./MC_stop_h3.root", "read")
    h3=f.Get("h3")
    h3.SetDirectory(0)
    _object_array=hist2array(h3)
 
    vol = Volume(_object_array).alpha([0.0, 0.0, 0.2, 0.25, 0.4, 0.4]).c('blue')# paper used
    cam = dict(pos=(500, 500, 500),
           focalPoint=(1000, 0, 0),
           distance=200.94)
    #plt=show(vol, __doc__, axes=1, interactive=False)
    plt=show(vol, __doc__, axes=1)
    #plt=show(vol, __doc__, axes=1,camera=cam, interactive=False)
    io.screenshot(filename="/Users/chiu.i-huan/Desktop/muonstop_3Dplot.png")

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-i","--inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/depth_build/Output_DEW12007_30MeV_2B.root", help="Input File")
    args = parser.parse_args()
    
    mkh3(args)
    main()
