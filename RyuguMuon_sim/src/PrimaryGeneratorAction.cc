//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "PrimaryGeneratorAction.hh"
#include "Parameters.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "RunAction.hh"
#include "G4GeneralParticleSource.hh"
#include "G4Geantino.hh"
#include "G4IonTable.hh"

G4int PrimaryGeneratorAction::fractionOfEletronParticles = 10;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0), 
  fParticleGunEle(0) 
{

   // ------------------------------------------------------------------------
   // get parameter
   // ------------------------------------------------------------------------
   //BeamType="muon";
   BeamType="gamma";
   //BeamType="ri";
   char tmpString0[100]="Unset", tmpString1[100]="Unset";
   if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
      char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
      FILE *fSteeringFile=fopen(charSteeringFileName,"r");
      char  line[501];
      while (!feof(fSteeringFile)) {
         fgets(line,500,fSteeringFile);
         if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
         sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
         if (strcmp(tmpString0,"/command/beamtype")==0){ BeamType = tmpString1;
         }
      }
   }

  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  if(BeamType == "muon"){
     G4cout << Form("Beam type is %s",BeamType.c_str()) << G4endl;
     G4ParticleDefinition* particle=particleTable->FindParticle("mu-");
     fParticleGun->SetParticleDefinition(particle);
     muon_mass = fParticleGun->GetParticleDefinition()->GetPDGMass();

     fParticleGunEle  = new G4ParticleGun(n_particle);
     G4ParticleDefinition* eletron=particleTable->FindParticle("e-");
     fParticleGunEle->SetParticleDefinition(eletron);
     ele_mass = fParticleGunEle->GetParticleDefinition()->GetPDGMass();
  }else if (BeamType == "gamma"){
     G4cout << Form("Beam type is %s",BeamType.c_str()) << G4endl;
     G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
     fParticleGun->SetParticleDefinition(particle);
     gamma_mass = fParticleGun->GetParticleDefinition()->GetPDGMass();
  }else if (BeamType == "ri"){
     fParticleGun->SetParticleEnergy(0*eV);
     fParticleGun->SetParticlePosition(G4ThreeVector(0.,0.,0.));
     fParticleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
     fParticleGun->SetParticlePosition(G4ThreeVector(0,0,275.15*CLHEP::mm));
     G4cout << Form("Beam type is %s",BeamType.c_str()) << G4endl;
  }else{
     BeamType = "ri";
     G4cout << Form("Set Beam type to %s",BeamType.c_str()) << G4endl;
  }
  count_event=0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fParticleGunEle;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  RootOutput* myRootOutput = RootOutput::GetRootInstance();
  myRootOutput->ClearAllRootVariables(); 

  if(BeamType == "muon"){
  // ***** muon beam ***** 
     // === particle incident position ===
     //gauss for x and y
     x0 = G4RandGauss::shoot(poi_mean,poi_sigmaX)*CLHEP::mm;
     y0 = G4RandGauss::shoot(poi_mean,poi_sigmaY)*CLHEP::mm;
     z0 = -5*CLHEP::mm;
     //temp: only for this case (cut for beam)
     //if (std::fabs(x0)>60) x0 = SetCutforBeam(x0,poi_sigmaX);
     //if (std::fabs(y0)>60) y0 = SetCutforBeam(y0,poi_sigmaY);
     fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
   
     // == default particle momentum ==
     pSigma = p0*mom_error;
     p = G4RandGauss::shoot(p0,pSigma)*MeV;
     G4double ux = p*dir_error_x*2*(G4UniformRand()-0.5)*MeV,
              uy = p*dir_error_y*2*(G4UniformRand()-0.5)*MeV,
              uz = std::sqrt(p*p - ux*ux - uy*uy)*MeV;
     fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum
     G4double particleEnergy = std::sqrt(p*p+muon_mass*muon_mass)-muon_mass;
     fParticleGun->SetParticleEnergy(particleEnergy);//IH 
     //this command will show previous particle energy and current particle momentum "EACH EVENT" !!
     //fParticleGun->SetParticleMomentum(p*MeV);//IH 
     fParticleGun->GeneratePrimaryVertex(anEvent);// === particle gen. ===
     G4double muInitTime = fParticleGun->GetParticleTime();
   
   // ***** electron *****
     //TODO try not to used random, very slow
     G4double ux_e = p*(dir_error_x/10)*2*(G4UniformRand()-0.5)*MeV,
              uy_e = p*(dir_error_y/10)*2*(G4UniformRand()-0.5)*MeV,
              uz_e = std::sqrt(p*p - ux_e*ux_e - uy_e*uy_e)*MeV;
     fParticleGunEle->SetParticleMomentumDirection(G4ThreeVector(ux_e,uy_e,uz_e));
     particleEnergy = std::sqrt(p*p+ele_mass*ele_mass)-ele_mass;
     fParticleGunEle->SetParticleEnergy(particleEnergy);//IH
     long thisEventNr = (long) (anEvent->GetEventID());
     if ((thisEventNr != 0) && (thisEventNr%fractionOfEletronParticles == 0)) {
       // ** gauss **
       x0_e = G4RandGauss::shoot(poi_mean,poi_sigmaX)*CLHEP::mm;
       y0_e = G4RandGauss::shoot(poi_mean,poi_sigmaY)*CLHEP::mm;
       //temp: only for this case (cut for beam)
       //if (std::fabs(x0_e)>60) x0_e = SetCutforBeam(x0_e,poi_sigmaX);
       //if (std::fabs(y0_e)>60) y0_e = SetCutforBeam(y0_e,poi_sigmaY);
       fParticleGunEle->SetParticlePosition(G4ThreeVector(x0_e,y0_e,z0));
       fParticleGunEle->GeneratePrimaryVertex(anEvent);// === particle gen. ===
     }
   
     myRootOutput->SetInitialMuonParameters(x0,y0,z0,ux,uy,uz,muInitTime);
     myRootOutput->SetInitialEletronParameters(x0_e,y0_e,z0,ux_e,uy_e,uz_e);

  }else if (BeamType == "gamma"){
  // ***** gamma *****
     // === SetMySeed ===
//     const auto p1 = std::chrono::system_clock::now();
//     auto start = std::chrono::high_resolution_clock::now();     
//     tpns tp = clock::now();
//     std::chrono::duration<double, std::nano> dur = tp.count();
//     std::chrono::time_point<std::chrono::high_resolution_clock> tp = std::chrono::high_resolution_clock::now();
//     auto dur = std::chrono::system_clock::to_time_t(std::chrono::time_point_cast<std::chrono::system_clock::duration>(tp));
//     std::chrono::duration<double, std::nano> dur = tp;
//     G4double nanos = dur.count();
//     std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(p1.time_since_epoch()).count() << std::endl;
//     std::cout << nanos << std::endl;
//     std::cout << G4UniformRand() << std::endl;
     //mySeed = std::time(0)/((1670300933+1.111)*10000+1.111); 
     // === RI source ===
     //x0_ga = 0;
     //y0_ga = 0;
     //z0_ga = 275.113*CLHEP::mm;
     // === Ryugu ===
     // NOTE: 275.113 mm is center of sample; 
     // 3.28 mm is thickness of ryugy sample; 
     // depth of stopped muon in Ryugu sample is 0.716409 (sigma = 0.150326) mm;    
     G4double length_x = 8.65; G4double length_y = 5.12;//Ryugu
     G4double z_temp=-0.5*3.28+G4RandGauss::shoot(0.716409,0.150326)*CLHEP::mm;//sample is at z=0
     G4double x_temp=(G4UniformRand()-0.5)*2*(length_x/2.)*CLHEP::mm; 
     z0_ga = 275.113+(std::cos(2*CLHEP::pi*(-20/360.)*CLHEP::rad)*z_temp - std::sin(2*CLHEP::pi*(-20/360.)*CLHEP::rad)*x_temp);
     x0_ga = std::sin(2*CLHEP::pi*(-20/360.)*CLHEP::rad)*z_temp+std::cos(2*CLHEP::pi*(-20/360.)*CLHEP::rad)*x_temp; 
     y0_ga = (G4UniformRand()-0.5)*2*(length_y/2.)*CLHEP::mm;

     fParticleGun->SetParticlePosition(G4ThreeVector(x0_ga,y0_ga,z0_ga));

     mySeed = std::time(0)/((1670300933+1.111)*10000+1.111);
     G4int p_flag=count_event%(4400+1);// 30~140 = 110 keV /0.025 keV = 4400
     gen_e_gamma =(mySeed+30.0+p_flag*0.025)*0.001;// p_flag is 0~4400; gen_e_gamma is up to 30 + 4400*0.025 = 30 + 110 keV
     //gen_e_gamma = (30+G4UniformRand()*110)*0.001;// random energy
     fParticleGun->SetParticleEnergy(gen_e_gamma);//set gamma energy
     myRootOutput->SetInitEnergy(gen_e_gamma);//set init. gamma energy

     // *** 4*pi direction ***
     //ux_ga = 2*(G4UniformRand()-0.5)*MeV;
     //uy_ga = 2*(G4UniformRand()-0.5)*MeV;
     //uz_ga = 2*(G4UniformRand()-0.5)*MeV;//random direction to xyz
     //fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux_ga,uy_ga,uz_ga));//Momentum

     // *** fixed theta and phi ***
     G4int ndir = int(G4UniformRand()*6); // 0~5
     //G4int ndir = 0+rand()%6; // 0~5
     G4double r = 1;
     G4double phi = ((2*CLHEP::pi)/360.)*(60+ndir*60+4*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range is +-8 (8/180)
     G4double theta = ((2*CLHEP::pi)/360.)*(90+4*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range is 8 (8/360)
     G4double ux = r*std::sin(theta)*std::cos(phi);
     G4double uy = r*std::sin(theta)*std::sin(phi);
     G4double uz = r*std::cos(theta);
     fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum

     // to 6-ch (no used)
     //int itheta = int(G4UniformRand()*6); //random 0~5
     //fParticleGun->SetParticleMomentumDirection(G4ThreeVector(std::sin(2*CLHEP::pi*(itheta*60+30)/360.*CLHEP::rad),std::cos(2*CLHEP::pi*(itheta*60+30)/360.*CLHEP::rad),0));

     fParticleGun->GeneratePrimaryVertex(anEvent);// === particle gen. ===
     myRootOutput->h2_init_energy->Fill(ndir+1,gen_e_gamma*1000);//Fill energy of input gamma

  }else{
  // ***** RI source *****
     if (fParticleGun->GetParticleDefinition() == G4Geantino::Geantino()) {
        //if not UI : Name and type is "geantino"
        //create vertex
        G4int Z = 27, A = 57;//init. source : Co57
        G4double excitEnergy = 0.*keV;
        G4ParticleDefinition* ion = G4IonTable::GetIonTable()->GetIon(Z,A,excitEnergy);
        fParticleGun->SetParticleDefinition(ion);
     }
     fParticleGun->GeneratePrimaryVertex(anEvent);
  }//beam type

  count_event++;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

