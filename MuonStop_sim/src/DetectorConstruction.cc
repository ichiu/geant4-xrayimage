//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "DetectorConstruction.hh"
#include "Parameters.hh"
#include "Messenger.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction()
{ 
   myMessenger = new Messenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
   delete myMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
   // ------------------------------------------------------------------------
   // get parameter
   // ------------------------------------------------------------------------
   SampleName="10";
   if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
      char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
      FILE *fSteeringFile=fopen(charSteeringFileName,"r");
      char  line[501];
      while (!feof(fSteeringFile)) {
         fgets(line,500,fSteeringFile);
         if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
         char tmpString0[100]="Unset", tmpString1[100]="Unset";
         sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
         if (strcmp(tmpString0,"/command/sample")==0){ SampleName = tmpString1;
         std::cout << "here: " << line[0] << " tmpString0 " << tmpString0 << tmpString1 << std::endl;
         }
      }
   }

  // ------------------------------------------------------------------------
  // Elements
  // ------------------------------------------------------------------------
  G4double A, Z;
  G4Element* elH  = new G4Element("Hydrogen","H",  Z=1.,  A=1.00794*g/mole);
  G4Element* elC  = new G4Element("Carbon",  "C",  Z=6.,  A= 12.011 *g/mole);
  G4Element* elN  = new G4Element("Nitrogen","N",  Z=7.,  A= 14.00674*g/mole);
  G4Element* elO  = new G4Element("Oxygen",  "O",  Z=8.,  A= 15.9994*g/mole);
  G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
  G4Element* elSi = new G4Element("Silicon", "Si", Z=14., A= 28.0855*g/mole);
  G4Element* elAr = new G4Element("Argon",   "Ar", Z=18., A= 39.948*g/mole);
  G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
  G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);
  G4Element* elMg  = new G4Element("Magnesium", "Mg",Z=12.,A= 24.3050    *g/mole);
  G4Element* elFe  = new G4Element("Iron",      "Fe",Z=26.,A= 55.847     *g/mole);

  G4NistManager* nist = G4NistManager::Instance();
  G4Material* W = nist->FindOrBuildMaterial("G4_W");
  G4Material* Cu = nist->FindOrBuildMaterial("G4_Cu");
  G4Material* elA = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* HeA = nist->FindOrBuildMaterial("G4_He");
  G4Material* SiO2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  G4Material* CaO = nist->FindOrBuildMaterial("G4_CALCIUM_OXIDE");
  G4Material* Al2O3 = nist->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* MgO = nist->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE");
  G4Material* FeO = nist->FindOrBuildMaterial("G4_FERROUS_OXIDE");
  G4Material* TiO2 = nist->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE");
  G4Material* Na2O = nist->FindOrBuildMaterial("G4_SODIUM_MONOXIDE");

  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  G4Material* Vacuum = new G4Material( "Vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure); 
  
  G4bool checkOverlaps = true;

  // ***** World *****
  G4double world_sizeXY = 60*cm;
  G4double world_sizeZ  = 60*cm;  
  G4Box* solidWorld = new G4Box("World",0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);      
  G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld,Vacuum,"World");                                       
  G4VPhysicalVolume* physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);                            

  G4double mydis = -219;//set sample surface to 0
  G4double Tubesize = 240.;//mm

  // ***** Beam foil (kapton) *****
  G4Material* solid_foil1 = nist->FindOrBuildMaterial("G4_KAPTON");
  G4double foil1_thick = 0.100;//mm
  G4VSolid* foil1_tubs = new G4Tubs("FoilTubs1",0*mm,(Tubesize/2.)*mm,(foil1_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_foil1 = G4ThreeVector(0, 0, (mydis+(foil1_thick/2.))*mm);//check beam position
  G4LogicalVolume* FoilLog1 = new G4LogicalVolume(foil1_tubs, solid_foil1, "FoilTubs1");  
  new G4PVPlacement(0, pos_foil1, FoilLog1, "FoilTubs1", logicWorld, false, 0,  checkOverlaps);        

  // ***** intermediate Air *****
  G4double inter_air_thick = 218;//mm
  G4VSolid* inter_air_tubs = new G4Tubs("intermediate1",0*mm,(Tubesize/2.)*mm,(inter_air_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_inter_air = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick/2.)*mm);//check beam position
  G4LogicalVolume* INTERLog1 = new G4LogicalVolume(inter_air_tubs, elA, "intermediate1");  
  new G4PVPlacement(0, pos_inter_air, INTERLog1, "intermediate1", logicWorld, false, 0,  checkOverlaps);        

  // ***** Cu *****
  G4double inter_check = stoi(SampleName)*0.01;//mm
  if (inter_check != 0){
    G4VSolid* check_tubs = new G4Tubs("Check",0*mm,((100)/2.)*mm,(inter_check/2.)*mm,0.,2*M_PI*rad);
    G4ThreeVector pos_check = G4ThreeVector(0, 0, (inter_check/2.)*mm);//check beam position
    G4LogicalVolume* CHECKLog = new G4LogicalVolume(check_tubs, Cu, "Check");  
    new G4PVPlacement(0, pos_check, CHECKLog, "Check", logicWorld, false, 0,  checkOverlaps);        
  }

  // ***** Target *****
  G4Material* solid_common;
  solid_common = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  G4double thinkness = (4/2.);
  G4double width = 30.;
  G4VSolid* Target = new G4Tubs("Target",0*mm,width*mm,thinkness*mm,0.,2*M_PI*rad);
  G4ThreeVector pos = G4ThreeVector(0, 0, (inter_check+thinkness)*mm);
  G4LogicalVolume* TargetLog = new G4LogicalVolume(Target, solid_common, "Target");
  new G4PVPlacement(0, pos, TargetLog, "Target", logicWorld, false, 0, checkOverlaps);


  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
