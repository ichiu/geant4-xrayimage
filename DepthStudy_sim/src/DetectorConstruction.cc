//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "DetectorConstruction.hh"
#include "Parameters.hh"
#include "Messenger.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction()
{ 
   myMessenger = new Messenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
   delete myMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
   // ------------------------------------------------------------------------
   // get parameter
   // ------------------------------------------------------------------------
   SampleName="Fe";
   if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
      char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
      FILE *fSteeringFile=fopen(charSteeringFileName,"r");
      char  line[501];
      while (!feof(fSteeringFile)) {
         fgets(line,500,fSteeringFile);
         if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
         char tmpString0[100]="Unset", tmpString1[100]="Unset";
         sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
         if (strcmp(tmpString0,"/command/sample")==0){ SampleName = tmpString1;
         std::cout << "here: " << line[0] << " tmpString0 " << tmpString0 << tmpString1 << std::endl;
         }
      }
   }

  // ------------------------------------------------------------------------
  // Elements
  // ------------------------------------------------------------------------
  G4double A, Z;
  G4Element* elH  = new G4Element("Hydrogen","H",  Z=1.,  A=1.00794*g/mole);
  G4Element* elC  = new G4Element("Carbon",  "C",  Z=6.,  A= 12.011 *g/mole);
  G4Element* elN  = new G4Element("Nitrogen","N",  Z=7.,  A= 14.00674*g/mole);
  G4Element* elO  = new G4Element("Oxygen",  "O",  Z=8.,  A= 15.9994*g/mole);
  G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
  G4Element* elSi = new G4Element("Silicon", "Si", Z=14., A= 28.0855*g/mole);
  G4Element* elAr = new G4Element("Argon",   "Ar", Z=18., A= 39.948*g/mole);
  G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
  G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);
  G4Element* elMg  = new G4Element("Magnesium", "Mg",Z=12.,A= 24.3050    *g/mole);
  G4Element* elFe  = new G4Element("Iron",      "Fe",Z=26.,A= 55.847     *g/mole);

  G4NistManager* nist = G4NistManager::Instance();
  G4Material* W = nist->FindOrBuildMaterial("G4_W");
  G4Material* elA = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* HeA = nist->FindOrBuildMaterial("G4_He");
  G4Material* SiO2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  G4Material* CaO = nist->FindOrBuildMaterial("G4_CALCIUM_OXIDE");
  G4Material* Al2O3 = nist->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* MgO = nist->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE");
  G4Material* FeO = nist->FindOrBuildMaterial("G4_FERROUS_OXIDE");
  G4Material* TiO2 = nist->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE");
  G4Material* Na2O = nist->FindOrBuildMaterial("G4_SODIUM_MONOXIDE");

  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  G4Material* Vacuum = new G4Material( "Vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure); 
  
  G4bool checkOverlaps = true;

  // ***** World *****
  G4double world_sizeXY = 60*cm;
  G4double world_sizeZ  = 60*cm;  
  G4Box* solidWorld = new G4Box("World",0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);      
  G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld,Vacuum,"World");                                       
  G4VPhysicalVolume* physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);                            

  G4double mydis = -275.12;//set sample surface to 0
  G4double Tubesize = 240.;//mm

  // ***** Beam foil (kapton) *****
  G4Material* solid_foil1 = nist->FindOrBuildMaterial("G4_KAPTON");
  G4double foil1_thick = 0.100;//mm
  G4VSolid* foil1_tubs = new G4Tubs("FoilTubs1",0*mm,(Tubesize/2.)*mm,(foil1_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_foil1 = G4ThreeVector(0, 0, (mydis+(foil1_thick/2.))*mm);//check beam position
  G4LogicalVolume* FoilLog1 = new G4LogicalVolume(foil1_tubs, solid_foil1, "FoilTubs1");  
  new G4PVPlacement(0, pos_foil1, FoilLog1, "FoilTubs1", logicWorld, false, 0,  checkOverlaps);        

  // ***** intermediate Air *****
  G4double inter_air_thick = 45;//mm
  G4VSolid* inter_air_tubs = new G4Tubs("intermediate1",0*mm,(Tubesize/2.)*mm,(inter_air_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_inter_air = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick/2.)*mm);//check beam position
  G4LogicalVolume* INTERLog1 = new G4LogicalVolume(inter_air_tubs, elA, "intermediate1");  
   new G4PVPlacement(0, pos_inter_air, INTERLog1, "intermediate1", logicWorld, false, 0,  checkOverlaps);        

  // ***** Chamber window foil (Al) *****
  G4Material* solid_foil2 = nist->FindOrBuildMaterial("G4_Al");
  G4double foil2_thick = 0.0125;//mm
  G4VSolid* foil2_tubs = new G4Tubs("FoilTubs2",0*mm,(Tubesize/2.)*mm,(foil2_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_foil2 = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick+(foil2_thick/2.))*mm);//check beam position
  G4LogicalVolume* FoilLog2 = new G4LogicalVolume(foil2_tubs, solid_foil2, "FoilTubs2");  
  new G4PVPlacement(0, pos_foil2, FoilLog2, "FoilTubs2", logicWorld, false, 0,  checkOverlaps);        

  // ***** intermediate He *****
  G4double inter_he_thick = 230;//mm
  G4VSolid* inter_he_tubs = new G4Tubs("intermediate2",0*mm,(Tubesize/2.)*mm,(inter_he_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_inter_he = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick+foil2_thick+(inter_he_thick/2.))*mm);//check beam position
  G4LogicalVolume* INTERLog2 = new G4LogicalVolume(inter_he_tubs, HeA, "intermediate2");  
  new G4PVPlacement(0, pos_inter_he, INTERLog2, "intermediate2", logicWorld, false, 0,  checkOverlaps);        
  
  // ***** Sample holder (kapton) *****
  //G4Material* solid_foil3 = nist->FindOrBuildMaterial("G4_KAPTON"); 
  //G4double foil3_thick = 0.0075;//mm
  G4Material* solid_foil3 = nist->FindOrBuildMaterial("G4_Cu");//Ryugu 
  G4double foil3_thick = 0.005;//Ryugu
  mydis = -275.12+0.0025;//Ryugu
  G4VSolid* foil3_tubs = new G4Tubs("FoilTubs3",0*mm,(Tubesize/2.)*mm,(foil3_thick/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_foil3 = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick+foil2_thick+inter_he_thick+(foil3_thick/2.))*mm);//check beam position
  G4LogicalVolume* FoilLog3 = new G4LogicalVolume(foil3_tubs, solid_foil3, "FoilTubs3");  
  new G4PVPlacement(0, pos_foil3, FoilLog3, "FoilTubs3", logicWorld, false, 0,  checkOverlaps);        

  // ***** Target *****
  G4Material* solid_common;
  double sample_density = 0*g/cm3;
  int ncomponents;
  double mass_fraction;
  G4double thinkness = (3.28/2.);//3.28 mm for Ryugu test
  G4double width = 100.;
  if (SampleName == "NWA482"){
     //White
     sample_density = 2.9*g/cm3;
     solid_common = new G4Material("White", sample_density, ncomponents=5);
     solid_common->AddMaterial(SiO2,mass_fraction=45*perCent);//mass fraction
     solid_common->AddMaterial(Al2O3,mass_fraction=30*perCent);
     solid_common->AddMaterial(CaO,mass_fraction=17*perCent);
     solid_common->AddMaterial(MgO,mass_fraction=4*perCent);
     solid_common->AddMaterial(FeO,mass_fraction=4*perCent);
  }else if (SampleName == "NWA032"){
     //Black
     sample_density = 2.9*g/cm3;
     solid_common = new G4Material("Black", sample_density, ncomponents=6);
     solid_common->AddMaterial(SiO2,mass_fraction=46*perCent);
     solid_common->AddMaterial(FeO,mass_fraction=24*perCent);
     solid_common->AddMaterial(CaO,mass_fraction=10*perCent);
     solid_common->AddMaterial(Al2O3,mass_fraction=9*perCent);
     solid_common->AddMaterial(MgO,mass_fraction=8*perCent);
     solid_common->AddMaterial(TiO2,mass_fraction=3*perCent);
  }else if (SampleName == "DEW12007"){
     //DEW & DEW35
     sample_density = 2.9*g/cm3;
     solid_common = new G4Material("DEW", sample_density, ncomponents=7);
     solid_common->AddMaterial(SiO2,mass_fraction=47.1*perCent);
     solid_common->AddMaterial(Al2O3,mass_fraction=18.3*perCent);
     solid_common->AddMaterial(CaO,mass_fraction=12.9*perCent);
     solid_common->AddMaterial(FeO,mass_fraction=12.6*perCent);
     solid_common->AddMaterial(MgO,mass_fraction=8.1*perCent);
     solid_common->AddMaterial(TiO2,mass_fraction=0.6*perCent);
     solid_common->AddMaterial(Na2O,mass_fraction=0.4*perCent);
  }else if (SampleName == "Ryugu1"){
     //Ryugu
     sample_density = 1.635*g/cm3;//old
     solid_common = new G4Material("Ryugu", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=2.168*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.108*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=1.050*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=43.52*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.31*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=11.56*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.20*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.06*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=2.94*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.19*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.98*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.09*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.29*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.17*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=21.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.24*perCent);
  }else if (SampleName == "Ryugu2"){
     //Ryugu
     sample_density = 1.81*g/cm3;//new
     solid_common = new G4Material("Ryugu", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=2.168*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.108*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=1.050*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=43.52*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.31*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=11.56*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.20*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.06*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=2.94*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.19*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.98*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.09*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.29*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.17*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=21.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.24*perCent);
  }else if (SampleName == "CI"){
     //CI
     sample_density = 2.11*g/cm3;
     solid_common = new G4Material("CI", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 1.86*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 4.13*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 46.976*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=9.517*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=0.837*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=10.774*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 5.36*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.884*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=18.562*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.1*perCent);
  }else if (SampleName == "CM"){
     //CM
     sample_density = 2.12*g/cm3;
     solid_common = new G4Material("CM", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 1.15*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 2.32*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 43.74*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=11.9*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.14*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.2*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 3.0*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=1.2*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=21.1*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.25*perCent);
  }else if (SampleName == "CV"){
     //CV
     sample_density = 2.95*g/cm3;
     solid_common = new G4Material("CV", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 0.28*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 0.49*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 38.02*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=14.7*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.69*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=15.9*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 2.12*perCent);
      solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=1.8*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=23.6*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.4*perCent);
  }else if (SampleName == "CO"){
     //CO
     sample_density = 2.95*g/cm3;
     solid_common = new G4Material("CO", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 0.1*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 0.49*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 37.75*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=14.5*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.4*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=15.8*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 2.18*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=1.57*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=24.8*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.41*perCent);
  }else if (SampleName == "H4"){
     //H4
     sample_density = 3.44*g/cm3;
     solid_common = new G4Material("H4", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 0.01*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 0.24*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 36.0*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=13.94*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.11*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=17.04*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 2.0*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=1.24*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=27.15*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.27*perCent);
  }else if (SampleName == "LL"){
     //LL
     sample_density = 3.21*g/cm3;
     solid_common = new G4Material("LL", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 0.17*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 0.39*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 40.094*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=15.3*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.18*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=18.646*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 2.1*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=1.3*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=19.8*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.02*perCent);
  }else if (SampleName == "EH"){
     //EH
     sample_density = 3.72*g/cm3;
     solid_common = new G4Material("EH", sample_density, ncomponents=10);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction= 0.01*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction= 0.38*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction= 31.81*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=10.84*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=0.81*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=16.73*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction= 5.6*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.85*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=31.1*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.87*perCent);
  }else if (SampleName == "Orguiel"){
     //Orguiel
     sample_density = 1.615*g/cm3;
     solid_common = new G4Material("Orguiel", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=3.460*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.295*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=1.937*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=46.45*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.50*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=9.60*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=0.86*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=10.57*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.09*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=5.35*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.05*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.91*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.04*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.26*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.19*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=18.35*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.07*perCent);
  }else if (SampleName == "Murray"){
     //Murray
     sample_density = 2.375*g/cm3;
     solid_common = new G4Material("Murray", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=2.168*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.108*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=1.050*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=43.52*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.31*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=11.56*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.20*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.06*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=2.94*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.19*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.98*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.09*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.29*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.17*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=21.07*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.24*perCent);
  }else if (SampleName == "Yamato"){
     //Yamato
     sample_density = 2.429*g/cm3;
     solid_common = new G4Material("Yamato", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=3.116*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.101*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=1.059*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=45.04*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.57*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=10.50*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.00*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.24*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.09*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=4.99*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.22*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=0.83*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.10*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.28*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.18*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=17.57*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.11*perCent);
  }else if (SampleName == "Tagishi"){
     //Tagishi
     sample_density = 1.959*g/cm3;
     solid_common = new G4Material("Tagishi", sample_density, ncomponents=17);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_C"),mass_fraction=6.729*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_N"),mass_fraction=0.182*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_H"),mass_fraction=0.745*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_O"),mass_fraction=40.91*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Na"),mass_fraction=0.21*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mg"),mass_fraction=11.30*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Al"),mass_fraction=1.23*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Si"),mass_fraction=13.63*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_P"),mass_fraction=0.13*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_S"),mass_fraction=2.67*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_K"),mass_fraction=0.11*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ca"),mass_fraction=2.22*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ti"),mass_fraction=0.05*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Cr"),mass_fraction=0.39*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Mn"),mass_fraction=0.27*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"),mass_fraction=18.13*perCent);
     solid_common->AddMaterial(nist->FindOrBuildMaterial("G4_Ni"),mass_fraction=1.09*perCent);
  }else if (SampleName == "Fe"){
     //Tagishi
     solid_common = nist->FindOrBuildMaterial("G4_Fe");
  }
  G4VSolid* Target = new G4Tubs("Target",0*mm,width*mm,thinkness*mm,0.,2*M_PI*rad);
  //mydis+foil1_thick+inter_air_thick+foil2_thick+inter_he_thick+foil3_thick = 0
  G4ThreeVector pos = G4ThreeVector(0, 0, (mydis+foil1_thick+inter_air_thick+foil2_thick+inter_he_thick+foil3_thick+thinkness)*mm);
  G4LogicalVolume* TargetLog = new G4LogicalVolume(Target, solid_common, "Target");
  new G4PVPlacement(0, pos, TargetLog, "Target", logicWorld, false, 0, checkOverlaps);

  // ***** Check Volumn *****
  G4double inter_check = 100;//mm
  G4VSolid* check_tubs = new G4Tubs("Check",0*mm,((Tubesize+100)/2.)*mm,(inter_check/2.)*mm,0.,2*M_PI*rad);
  G4ThreeVector pos_check = G4ThreeVector(0, 0, (thinkness*2+inter_check/2.+5)*mm);//check beam position
  G4LogicalVolume* CHECKLog = new G4LogicalVolume(check_tubs, Vacuum, "Check");  
//  new G4PVPlacement(0, pos_check, CHECKLog, "Check", logicWorld, false, 0,  checkOverlaps);        

  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
