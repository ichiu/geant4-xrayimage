//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"
#include "RootOutput.hh"
using  namespace  std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
: G4UserSteppingAction(),
  fEventAction(eventAction)
//  fScoringVolume(0),
//  fScoringVolumeUp(0),
//  fSampleVolume(0)
{
  pointer=this;
  myRootOutput = RootOutput::GetRootInstance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

SteppingAction* SteppingAction::pointer=0;
SteppingAction* SteppingAction::GetInstance()
{
  return pointer;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::InitializeInBeginningOfEvent(){

  VolumeMap["World"] = 0;
  VolumeMap["Target"] = 1;
  VolumeMap["FoilTubs1"] = 2;
  VolumeMap["intermediate1"] = 3;
  VolumeMap["FoilTubs2"] = 4;
  VolumeMap["intermediate2"] = 5;
  VolumeMap["FoilTubs3"] = 6;
  VolumeMap["Check"] = -1;

  ProcessMap["muMinusCaptureAtRest"] = 1;
  ProcessMap["phot"] = 2;
  ProcessMap["compt"] = 3;
  ProcessMap["eBrem"] = 4;
  ProcessMap["neutronInelastic"] = 5;
  ProcessMap["muIoni"] = 6;
  ProcessMap["conv"] = 7;
  muhitSampleInThisEvent = false;
  GammahitCheckInThisEvent = false;
  ngammaHitVolume = 0;
  neletronHitVolume = 0;
  nneutronHitVolume = 0;
  notherHitVolume = 0;
  ParticleID = 0;
  ParentID = 0;
  EnergyDeposit = 0; 
  EnergyDeposit_gamma = 0; 
  EnergyDeposit_e = 0; 
  EnergyDeposit_other = 0; 
  egamma_hittime = -1000; 
  currentType = 0;
  for (int i = 0; i< nhitMax_indetector; i++){
     ahit_edep[i] = 0.;
     ahit_start_x[i] = -100.;
     ahit_start_y[i] = -100.;
     ahit_start_z[i] = -100.;
     ahit_time_start[i] = 0.;
     ahit_time_end[i] = 0.; 
     ahit_nsteps[i] = 0;
     ahit_pdgid[i] = 0;
     ahit_process[i] = 0;
  }
  nSignals=0;//number of signal particles
  IsSameSignal = false;//same signal, depend on time resolution of detector
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  // get volume of the current step
  aTrack        = step->GetTrack();
  if (aTrack->GetDefinition()){

     volume = aTrack->GetVolume()->GetLogicalVolume();
     CurrentVolumeName = volume->GetName();
     pdgID         = aTrack->GetDefinition()->GetPDGEncoding();
     TrackPosition = aTrack->GetPosition();
     TrackMomentum = aTrack->GetMomentum();//three vector
     particleName  = aTrack->GetDefinition()->GetParticleName();
     ParentID      = aTrack->GetParentID();
     Time          = aTrack->GetGlobalTime()/CLHEP::microsecond;
     KineticEnergy = aTrack->GetKineticEnergy()/CLHEP::MeV;
//     std::cout << CurrentVolumeName << "  pdgID : "<< pdgID << " e: " << KineticEnergy << " par: " << ParentID << std::endl;

     G4StepPoint* preStepPoint = step->GetPreStepPoint();
     G4StepPoint* postStepPoint = step->GetPostStepPoint();
     G4ThreeVector preStepPosition = preStepPoint->GetPosition();
     G4ThreeVector postStepPosition = postStepPoint->GetPosition();

     // =========== store muon hit position ===============    
     if(particleName == "mu-"){// note: before touch physic volume, pdgID is random number
//        std::cout << KineticEnergy << "   " <<TrackPosition.z() <<  "  pre : " << preStepPosition.z() << "  post : " << postStepPosition.z() <<std::endl;
        if (!muhitSampleInThisEvent) {//start point
           muhitSampleInThisEvent = true;
           myRootOutput->SetInitPolInSample(TrackPosition);
           myRootOutput->SetInitMomInSample(TrackMomentum);
           myRootOutput->SetInitTimeInSample(Time);
           myRootOutput->SetInitKineticEnergyInSample(KineticEnergy);
        }else{//end point 
           myRootOutput->SetEndPolInSample(TrackPosition);
           myRootOutput->SetEndMomInSample(TrackMomentum);
           myRootOutput->SetEndTimeInSample(Time);            
           myRootOutput->SetEndKineticEnergyInSample(KineticEnergy);            
        }
     }//muon end


     if (TrackPosition.z() > 3.28 && !GammahitCheckInThisEvent && pdgID == 22) {
     //if (VolumeMap[CurrentVolumeName] == -1 && !GammahitCheckInThisEvent && pdgID == 22) {
        //myRootOutput->h1_hit_energy->Fill(KineticEnergy*1000);//same preStepPoint->GetKineticEnergy() with when volumn is Vacuum -> no energy deposit
        myRootOutput->h1_hit_energy->Fill(preStepPoint->GetKineticEnergy()*1000);// 
        GammahitCheckInThisEvent = true;
     }

  }// end : if (aTrack->GetDefinition())
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

