//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "RunAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0), 
  fParticleGunEle(0), 
  fEnvelopeBox(0)
//  t0(0), tSigma(0), 
//  x0(0), y0(0), z0(-10*CLHEP::cm)
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  fParticleGunEle  = new G4ParticleGun(n_particle);

  //define input particles
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
//    = particleTable->FindParticle(particleName="mu-");//IH
    = particleTable->FindParticle(particleName="gamma");//IH
  fParticleGun->SetParticleDefinition(particle);
  particle_mass = fParticleGun->GetParticleDefinition()->GetPDGMass();
  count_event=0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fParticleGunEle;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of ecah event
  //

  // In order to avoid dependence of PrimaryGeneratorAction
  // on DetectorConstruction class we get Envelope volume
  // from G4LogicalVolumeStore.

  RootOutput* myRootOutput = RootOutput::GetRootInstance();
  myRootOutput->ClearAllRootVariables(); 

  /* 
  //=== default particle kinematic (muon) === 
  //G4double p0=15+int(count_event%46);// 15+0 ~ 15+45
  G4double p0=29.2;// 27 MeV/c only for Ryugu paper
  p=p0;
  pSigma = p0*mom_error;
  p = G4RandGauss::shoot(p0,pSigma)*MeV;
  px = 0;
  py = 0;
  pz = std::sqrt(p*p - px*px - py*py);

  // === particle init. position ===
  x0 = 0*CLHEP::mm;
  y0 = 0*CLHEP::mm;
  z0 = -299*CLHEP::mm;//-300 is world border
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));

  // === particle init. momentum and energy ===
  // *** fixed theta and phi
  //G4int ndir = count_event%6; // 0~5
  //G4double r = 1;
  //G4double phi = ((2*CLHEP::pi)/360.)*(60+ndir*60+10*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range is +-20
  //G4double theta = ((2*CLHEP::pi)/360.)*(90+10*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range is 20
  //G4double ux = r*std::sin(theta)*std::cos(phi);
  //G4double uy = r*std::sin(theta)*std::sin(phi);
  //G4double uz = r*std::cos(theta);
  //fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum

  G4double e0 = std::sqrt(p*p+particle_mass*particle_mass)-particle_mass;
  //eSigma = e0*energy_error;
  //particleEnergy = G4RandGauss::shoot(e0,eSigma);
  particleEnergy = e0;
  fParticleGun->SetParticleEnergy(particleEnergy);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(px,py,pz));//Momentum
  fParticleGun->GeneratePrimaryVertex(anEvent);
  G4double muInitTime = fParticleGun->GetParticleTime()/CLHEP::nanosecond;

  myRootOutput->SetInitialMuonParameters(x0,y0,z0,0,0,p,muInitTime);
  // */

//  /*
  // gamma gen. for Ryugu absorption 
  // stopping depth for 29.2 MeV muon, 0.716409 +- 0.150326
  px = 0;
  py = 0;
  pz = 1;
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(px,py,pz));//Direction

  x0 = 0*CLHEP::mm;
  y0 = 0*CLHEP::mm;
  //z0 = G4RandGauss::shoot(3.28-0.716409,0.150326)*CLHEP::mm;//shallow
  //z0 = G4RandGauss::shoot(0.716409,0.150326)*CLHEP::mm;//deep
  //z0 = G4RandGauss::shoot(3.28-0.76,0.13)*CLHEP::mm;//shallow
  //z0 = G4RandGauss::shoot(0.76,0.13)*CLHEP::mm;//deep
  //z0 = G4RandGauss::shoot(3.28-0.202181,0.0440971)*CLHEP::mm;//shallow
  z0 = G4RandGauss::shoot(3.28-(0.5-0.202181),0.0440971)*CLHEP::mm;//deep
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));

  //G4int p_flag=count_event%(7800+1);// 5~200 = 195 keV /0.025 keV = 7800
  //gen_e_gamma =(5+p_flag*0.025)*0.001;// p_flag is 0~7800; gen_e_gamma is up to 5 + 7800*0.025 = 5 + 195 keV
  //gen_e_gamma = (30 + 110*G4UniformRand())*0.001; //5 ~ 195 keV
  gen_e_gamma = (40)*0.001; //5 ~ 195 keV

  fParticleGun->SetParticleEnergy(gen_e_gamma);//set gamma energy
  fParticleGun->GeneratePrimaryVertex(anEvent);// === particle gen. ===
  myRootOutput->h1_init_energy->Fill(gen_e_gamma*1000);//Fill energy of input gamma
  // */

  count_event++;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

