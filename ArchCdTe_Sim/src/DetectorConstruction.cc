//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "DetectorConstruction.hh"
#include "Messenger.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction()
{ 
   myMessenger = new Messenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
   delete myMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  

  // ------------------------------------------------------------------------
  // Elements
  // ------------------------------------------------------------------------
  G4double A, Z;
  G4Element* elH  = new G4Element("Hydrogen","H",  Z=1.,  A=1.00794*g/mole);
  G4Element* elC  = new G4Element("Carbon",  "C",  Z=6.,  A= 12.011 *g/mole);
  G4Element* elN  = new G4Element("Nitrogen","N",  Z=7.,  A= 14.00674*g/mole);
  G4Element* elO  = new G4Element("Oxygen",  "O",  Z=8.,  A= 15.9994*g/mole);
  G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
  G4Element* elSi = new G4Element("Silicon", "Si", Z=14., A= 28.0855*g/mole);
  G4Element* elAr = new G4Element("Argon",   "Ar", Z=18., A= 39.948*g/mole);
  G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
  G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);
  G4Element* elMg  = new G4Element("Magnesium", "Mg",Z=12.,A= 24.3050    *g/mole);
  G4Element* elFe  = new G4Element("Iron",      "Fe",Z=26.,A= 55.847     *g/mole);

  G4NistManager* nist = G4NistManager::Instance();
  G4Material* elA = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* HeA = nist->FindOrBuildMaterial("G4_He");
  G4Material* SiO2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  G4Material* CaO = nist->FindOrBuildMaterial("G4_CALCIUM_OXIDE");
  G4Material* Al2O3 = nist->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* MgO = nist->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE");
  G4Material* FeO = nist->FindOrBuildMaterial("G4_FERROUS_OXIDE");
  G4Material* TiO2 = nist->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE");
  G4Material* Na2O = nist->FindOrBuildMaterial("G4_SODIUM_MONOXIDE");
  G4double atomicNumber = 1.;
  G4double massOfMole = 1.008*g/mole;
  G4double density = 1.e-25*g/cm3;
  G4double temperature = 2.73*kelvin;
  G4double pressure = 3.e-18*pascal;
  G4Material* Vacuum = new G4Material( "Vacuum", atomicNumber, massOfMole, density, kStateGas, temperature, pressure); 
  
  G4bool checkOverlaps = true;

  // ***** World *****
  G4double world_sizeXY = 60*cm;
  G4double world_sizeZ  = 60*cm;  
  G4Box* solidWorld = new G4Box("World",0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);      
  G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld,Vacuum,"World");                                       
  G4VPhysicalVolume* physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);                            

  // ***** Collimator *****
  G4Material* shape_mat = nist->FindOrBuildMaterial("G4_W");
  // Conical section shape       
  G4double shape_rmina =  0., shape_rmaxa = 1.;//for radiu
  G4double shape_rminb =  0., shape_rmaxb = 3.5;
  G4int shape_hz = 8.26/4.;//8 mm for collimator thickness
  G4double shape_phimin = 0.*deg, shape_phimax = 360.*deg;
  G4Cons* solidShape =  new G4Cons("Shape", shape_rmina, shape_rmaxa, shape_rminb, shape_rmaxb, shape_hz, shape_phimin, shape_phimax);
  G4int colli_radiu = 32;// 64 mm for radiu
  G4VSolid* colliBody = new G4Tubs("Body",0*mm,colli_radiu*mm,shape_hz*2*mm,0.,2*M_PI*rad);

  G4LogicalVolume* logicShape1 = new G4LogicalVolume(solidShape, shape_mat, "Shape1");         
  G4LogicalVolume* logicShape2 = new G4LogicalVolume(solidShape, shape_mat, "Shape2");         
  G4RotationMatrix* angle_shape = new G4RotationMatrix(0,180*CLHEP::deg,0*CLHEP::deg);
  G4VSolid* all_shape = new G4UnionSolid("AllShape", solidShape, solidShape, angle_shape, G4ThreeVector(0, 0, -2*shape_hz)); 
  // === check ===
  //G4LogicalVolume* logicShapeall = new G4LogicalVolume(all_shape, shape_mat, "AllShape");        
  //new G4PVPlacement(0, G4ThreeVector(0, 0, shape_hz), logicShapeall, "AllShape", logicWorld, false, 0, checkOverlaps);
  //G4LogicalVolume* logicBody = new G4LogicalVolume(colliBody, shape_mat, "Body");         
  //new G4PVPlacement(0, G4ThreeVector(0, 50, 0), logicBody, "Body", logicWorld, false, 0, checkOverlaps);

  G4VSolid* collimator = new G4SubtractionSolid("Collimator", colliBody, all_shape, 0, G4ThreeVector(0., 0., shape_hz));
  G4LogicalVolume* logicCollimator = new G4LogicalVolume(collimator, shape_mat, "Collimator");         
  G4RotationMatrix* angle_collimator = new G4RotationMatrix(0,90*CLHEP::deg,0*CLHEP::deg);
  G4RotationMatrix* angle_collimator2 = new G4RotationMatrix(90*CLHEP::deg,90*CLHEP::deg,90*CLHEP::deg);
  new G4PVPlacement(angle_collimator , G4ThreeVector(0, sample_dis*mm, 0), logicCollimator, "Collimator", logicWorld, false, 0, checkOverlaps);
  new G4PVPlacement(angle_collimator , G4ThreeVector(0, (-1)*(sample_dis)*mm, 0), logicCollimator, "Collimator", logicWorld, false, 0, checkOverlaps);
  new G4PVPlacement(angle_collimator2 , G4ThreeVector((sample_dis)*mm, 0, 0), logicCollimator, "Collimator", logicWorld, false, 0, checkOverlaps);
  new G4PVPlacement(angle_collimator2 , G4ThreeVector((-1)*(sample_dis)*mm, 0, 0), logicCollimator, "Collimator", logicWorld, false, 0, checkOverlaps);

  // ***** Detector *****
  G4double det_dis = det_dis_temp+sample_dis+1;//from collimator center to detector center + collimator center to sample center + detector's thickness
  G4Material* solid_cryostat = nist->FindOrBuildMaterial("G4_CADMIUM_TELLURIDE");
  G4Box* cdte_tubs4 = new G4Box("CdTeTubs4",(32/2.)*mm,(2/2.)*mm,(32/2.)*mm); // 2 mm thickness
  G4Box* cdte_tubs2 = new G4Box("CdTeTubs2",(32/2.)*mm,(2/2.)*mm,(32/2.)*mm); // 2 mm thickness
  G4Box* cdte_tubs1 = new G4Box("CdTeTubs1",(2/2.)*mm,(32/2.)*mm,(32/2.)*mm); // 2 mm thickness
  G4Box* cdte_tubs3 = new G4Box("CdTeTubs3",(2/2.)*mm,(32/2.)*mm,(32/2.)*mm); // 2 mm thickness
  G4ThreeVector pos_cdte1 = G4ThreeVector(0*mm, (-1)*det_dis*mm, 0*mm);// collimator and detector's thickness
  G4ThreeVector pos_cdte2 = G4ThreeVector(0*mm, det_dis*mm, 0*mm);// collimator and detector's thickness
  G4ThreeVector pos_cdte3 = G4ThreeVector((-1)*det_dis*mm, 0*mm, 0*mm);// collimator and detector's thickness
  G4ThreeVector pos_cdte4 = G4ThreeVector(det_dis*mm, 0*mm, 0*mm);// collimator and detector's thickness
  G4LogicalVolume* CdTeLog1 = new G4LogicalVolume(cdte_tubs4, solid_cryostat, "CdTeTubs4");  
  G4LogicalVolume* CdTeLog2 = new G4LogicalVolume(cdte_tubs2, solid_cryostat, "CdTeTubs2");  
  G4LogicalVolume* CdTeLog3 = new G4LogicalVolume(cdte_tubs1, solid_cryostat, "CdTeTubs1");  
  G4LogicalVolume* CdTeLog4 = new G4LogicalVolume(cdte_tubs3, solid_cryostat, "CdTeTubs3");  
  new G4PVPlacement(0, pos_cdte1, CdTeLog1, "CdTeTubs4", logicWorld, false, 0,  checkOverlaps);        
  new G4PVPlacement(0, pos_cdte2, CdTeLog2, "CdTeTubs2", logicWorld, false, 0,  checkOverlaps);        
  new G4PVPlacement(0, pos_cdte3, CdTeLog3, "CdTeTubs1", logicWorld, false, 0,  checkOverlaps);        
  new G4PVPlacement(0, pos_cdte4, CdTeLog4, "CdTeTubs3", logicWorld, false, 0,  checkOverlaps);        

  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
