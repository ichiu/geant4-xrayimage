//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "PrimaryGeneratorAction.hh"
#include "Parameters.hh"
#include "Messenger.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "RunAction.hh"
#include "G4Geantino.hh"
#include "G4IonTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0), 
  fParticleGunEle(0), 
  fEnvelopeBox(0)
{
   pointerToPrimary=this;
   // ------------------------------------------------------------------------
   // get parameter
   // ------------------------------------------------------------------------
   SampleName="1";
   if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
      char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
      FILE *fSteeringFile=fopen(charSteeringFileName,"r");
      char  line[501];
      while (!feof(fSteeringFile)) {
         fgets(line,500,fSteeringFile);
         if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
         char tmpString0[100]="Unset", tmpString1[100]="Unset";
         sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
         if (strcmp(tmpString0,"/command/sample")==0){ SampleName = tmpString1;
         std::cout << "here: " << line[0] << " tmpString0 " << tmpString0 << tmpString1 << std::endl;
         }
      }
   }

  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();  //define input particles
  particleEnergy = (120+stoi(SampleName)*0.001) * 0.001;// 120 keV
  count_event=0;

//  *** muon ***
//  G4ParticleDefinition* particle = particleTable->FindParticle("mu-");//IH
//  fParticleGun->SetParticleDefinition(particle);
//  *** gamma ***
  G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
  fParticleGun->SetParticleDefinition(particle);
  particle_mass = fParticleGun->GetParticleDefinition()->GetPDGMass();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fParticleGunEle;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction* PrimaryGeneratorAction::pointerToPrimary=0;
PrimaryGeneratorAction* PrimaryGeneratorAction::GetPrimaryInstance() {
  return pointerToPrimary;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  // *** this function is called at the begining of ecah event ***
  RootOutput* myRootOutput = RootOutput::GetRootInstance();
  myRootOutput->ClearAllRootVariables(); 

  // === default particle kinematic ===
  fParticleGun->SetParticleEnergy(particleEnergy);//IH

  ///* baseline : cdte204
  // === particle init. position (x0, y0, z0 are int) ===
  //G4int ndir = count_event%(7*7*7);// 343 points for one loop
  //x0 = int(ndir/49);// 0 ~ 6
  //y0 = int((ndir - x0*49)/7);// 0 ~ 6
  //z0 = ndir - x0*49 - y0*7;// 0 ~ 6

  G4int ndir = count_event%(31*31*31);// 29791 points for one loop
  x0 = int(ndir/(31*31));//0 ~ 30
  y0 = int((ndir - x0*(31*31))/31);//0 ~ 30
  z0 = ndir - x0*(31*31) - y0*31;//0 ~ 30

  x0_index = x0; 
  y0_index = y0; 
  z0_index = z0; 
  x0 = x0 - 15 + 0;// -15 ~ 15
  y0 = y0 - 15 + 0;
  z0 = z0 - 15 + 0;
  //std::cout << "(X,Y,Z) = "<< x0<<" "<< y0<< " " << z0 << std::endl;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
  // === particle init. momentum and energy ===
  // *** theta & phi ***
  G4double r = 1;
  G4double phi = ((2*CLHEP::pi)/360.)*(90+55*(G4UniformRand()-0.5)*2)*CLHEP::rad;// 35 ~ 145 (ratio: 110/180)
  G4double theta = ((2*CLHEP::pi)/360.)*(270+55*(G4UniformRand()-0.5)*2)*CLHEP::rad;// 215 ~ 325 (ratio: 110/360)
  ux = r*std::sin(theta)*std::cos(phi);
  uy = r*std::sin(theta)*std::sin(phi);
  uz = r*std::cos(theta);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum
  // */


  /* === test sample ===
  // === circle ===
  //G4double thetasample = (2*CLHEP::pi)*G4UniformRand()*CLHEP::rad;
  //x0 = 5*std::sin(thetasample);
  //z0 = 5*std::cos(thetasample);
  // === line ===
  x0 = 5+(G4UniformRand()-0.5)*10;// 0 ~ 10
  z0 = 5; // 5
  // === point ===
  //x0 = 5;
  //z0 = 5; 

  y0 = 5;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
  // === particle init. momentum and energy ===
  // *** theta & phi ***
  G4int ndir = int(G4UniformRand()*4);//0 ~ 3
  G4double r = 1;
  G4double phi = ((2*CLHEP::pi)/360.)*(90*ndir+30*(G4UniformRand()-0.5)*2)*CLHEP::rad;// 35 ~ 145 (ratio: 110/180)
  G4double theta = ((2*CLHEP::pi)/360.)*(90+30*(G4UniformRand()-0.5)*2)*CLHEP::rad;// 215 ~ 325 (ratio: 110/360)
  ux = r*std::sin(theta)*std::cos(phi);
  uy = r*std::sin(theta)*std::sin(phi);
  uz = r*std::cos(theta);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum
  myRootOutput->h3_sample->Fill(x0,y0,z0);
  // */

  fParticleGun->GeneratePrimaryVertex(anEvent);
  myRootOutput->SetInitialParticleParameters(x0,y0,z0);
  count_event++;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

