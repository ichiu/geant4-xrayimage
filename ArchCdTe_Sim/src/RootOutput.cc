#include "RootOutput.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Parameters.hh"

RootOutput::RootOutput() {
  TTree::SetMaxTreeSize(100000000000LL);      // Set maximum size of the tree file            
  pointerToRoot=this;
}

RootOutput::~RootOutput() {}

RootOutput* RootOutput::pointerToRoot=0;
RootOutput* RootOutput::GetRootInstance() {
  return pointerToRoot;
}

void RootOutput::BeginOfRunAction() {
   // ------------------------------------------------------------------------
   // get parameter
   // ------------------------------------------------------------------------
   std::stringstream ss;ss.str("t");
   std::stringstream ss2;ss2.str("test");
   if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
      char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
      FILE *fSteeringFile=fopen(charSteeringFileName,"r");
      char  line[501];
      G4cout << "\n\n....oooOO0OOooo........oooOO0OOooo......Configuration file used for this run....oooOO0OOooo........oooOO0OOooo......"<<G4endl;
      while (!feof(fSteeringFile)) {
         fgets(line,500,fSteeringFile);
         if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
         char tmpString0[100]="Unset", tmpString1[100]="Unset";
         sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
         if (strcmp(tmpString0,"/command/sample")==0) ss << tmpString1;
      }
      G4cout <<"....oooOO0OOooo........oooOO0OOooo......End of the configuration file.....oooOO0OOooo........oooOO0OOooo......"<<G4endl;
   }

   auto ima_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
   ss2 << std::put_time(std::localtime(&ima_time), "%Y%m%d_%H%M%S");
   auto RootOutputFileName = "./Output_"+ss.str()+"_"+ss2.str()+".root";
   rootFile = new TFile(RootOutputFileName.c_str(), "recreate");
   G4cout << "....oooOO0OOooo........oooOO0OOooo......RE-create file.....oooOO0OOooo........oooOO0OOooo......" <<G4endl; 
   rootFile->cd();
   rootTree = new TTree("tree","CdTe detector analysis");

   rootTree->Branch("eventID",&eventID,"eventID/I");
   rootTree->Branch("runID",&runID,"runID/I"); 
   // ===== photon position =====
   rootTree->Branch("pInitX",&pInitX,"pInitX/I");//init particle position 
   rootTree->Branch("pInitY",&pInitY,"pInitY/I");//init particle position 
   rootTree->Branch("pInitZ",&pInitZ,"pInitZ/I");//init particle position 

   // ===== cdte response =====
   rootTree->Branch("nSignals",&nSignals,"nSignals/I");//nsiganle in a event
   rootTree->Branch("Hit_Energy",&hit_energy,"Hit_Energy[nSignals]/D"); 
   rootTree->Branch("Hit_Start_X",&hit_startx,"Hit_Start_X[nSignals]/D");
   rootTree->Branch("Hit_Start_Y",&hit_starty,"Hit_Start_Y[nSignals]/D");
   rootTree->Branch("Hit_Start_Z",&hit_startz,"Hit_Start_Z[nSignals]/D");

   h1_process = new TH1F("hit_process","Process of Signal",nh1bin,0,nh1bin);
   for (int i=1;i<=nh1bin;i++) h1_process->GetXaxis()->SetBinLabel(i,var_name[i-1]);
   for (int ix=0; ix < 31; ix++){
      for (int iy=0; iy < 31; iy++){
         for (int iz=0; iz < 31; iz++){
            sprintf(name, "image_X%i_Y%i_Z%i",ix,iy,iz);
            h2_image[ix][iy][iz] = new TH2F(name,name,128,-16,16,128,-16,16);
   }}}
   for (int idet=0; idet < 4; idet++){
      sprintf(name, "image_CdTe%i",idet+1);
      h2_sample[idet] = new TH2F(name,name,128,-16,16,128,-16,16);
   }
   h3_sample = new TH3F("h3_sample","h3_sample",30,-15,15,30,-15,15,30,-15,15);

   G4cout << "RootOutput::BeginOfRunAction()  The Root tree and branches were defined."<<G4endl;
}

void RootOutput::EndOfRunAction() {
  G4cout<<"....oooOO0OOooo........oooOO0OOooo......RootOutput::EndOfRunAction() - Write Tree....oooOO0OOooo........oooOO0OOooo......"<<G4endl;
  rootTree->Write();
  // TODO for response
  for (int ix=0; ix < 31; ix++){
    for (int iy=0; iy < 31; iy++){
       for (int iz=0; iz < 31; iz++){
          h2_image[ix][iy][iz]->Write();}}}

  //for (int idet=0; idet < 4; idet++){h2_sample[idet]->Write();}
  //h3_sample->Write();
  rootFile->Close();
  G4cout<<Form("....oooOO0OOooo........oooOO0OOooo......RootOutput::EndOfRunAction() - Root tree written out in %s....oooOO0OOooo........oooOO0OOooo......",rootFile->GetName())<<G4endl;
}

void RootOutput::FillEvent() {
  double total_E = 0;
  for (int i = 0; i < nSignals; i++) total_E+=hit_energy[i];
  if(total_E != 0) rootTree->Fill();//for energy deposit
}

void RootOutput::FillParticle() {
  TrackTree->Fill();
}

void RootOutput::SetEnergyResolution (){
   for (int i = 0; i < nSignals; i++){
//      hit_energy_reso[i]= G4RandGauss::shoot(hit_energy[i],(reso_init + hit_energy[i]*reso_rate));
      hit_energy_reso[i]= G4RandGauss::shoot(hit_energy[i],(0.152/1000. + (hit_energy[i]-0.024)*0.0010545454545454547));
   }
}

void RootOutput::ClearAllRootVariables() {
  runID=-1000; eventID=-1000;
  pInitX = -1000; pInitY = -1000;pInitZ = -1000;

  for (int i = 0; i < nhitMax; i++){
   hit_energy[i] = 0.;
   hit_startx[i] = 0.;
   hit_starty[i] = 0.;
   hit_startz[i] = 0.;
  }
}
