#ifndef RootOutput_h
#define RootOutput_h 1
//#include "G4UserRunAction.hh"
#include <CLHEP/Units/PhysicalConstants.h>
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "EventAction.hh"

//  ROOT
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TVectorD.h"
//
#include <map>

#include "G4ios.hh"
#include <fstream>
#include <vector>
#include <cmath>

class RootOutput{
  public:
    RootOutput();
    ~RootOutput();
    static RootOutput* GetRootInstance();

    void BeginOfRunAction();
    void EndOfRunAction();
    void FillEvent();
    void FillParticle();
    void ClearAllRootVariables();

    void SetRunID          (G4int id) {runID = id;};
    void SetEventID        (G4int id) {eventID = id;};

    static const int numberOfvolume = 10; 
    static const int nh1bin = 10;
    static const int nhitMax = 100;

    void SetInitialParticleParameters(G4int x,G4int y,G4int z){
         pInitX=x; pInitY=y; pInitZ=z;}

    void SetEnergyResolution ();

    TFile* rootFile;
    TTree* rootTree;
    TTree* TrackTree;

    TH1F*  h1_process;
    TH1F* h1_StopVol;
    char name[100] = "";
    TH2F* h2_image[31][31][31];
    TH2F* h2_sample[4];
    TH3F* h3_sample;

  // === variables for tree ===
  private:
    static RootOutput* pointerToRoot;
    const char *var_name[nh1bin] = {"None","muMinusCaptureAtRest","phot","compt","eBrem",
                                    "neutronInelastic","muIoni","conv","None","None"};

    Int_t runID;
    Int_t eventID;
    Int_t pInitX ; Int_t pInitY ;Int_t pInitZ ; 
    Double_t RunTime;

    Int_t nSignals;
    Int_t hit_id;
    Int_t Det_ID[nhitMax];
    Double_t hit_energy[nhitMax];
    Double_t hit_energy_reso[nhitMax];
    Double_t hit_startx[nhitMax];
    Double_t hit_starty[nhitMax];
    Double_t hit_startz[nhitMax];
    Double_t hit_timestart[nhitMax];
    Double_t hit_timeend[nhitMax];
    Int_t hit_nsteps[nhitMax];
    Double_t hit_length[nhitMax];
    Int_t hit_pdgId[nhitMax];
    Int_t hit_process[nhitMax];

   // === public class to catch info. === 
  public:
    void SetDetectorInfo (G4double edep, G4double edep_e, G4double edep_gamma, G4double edep_other, G4double time);
    void SetnMaxHit (G4int nhits){nSignals = nhits;}
    void SetRunTime (G4double time) {RunTime = time;}
    void SetSignalInfo (G4int det_id, G4int id, G4double energy, G4double start_x, G4double start_y, G4double start_z, G4double time_start, G4double time_end, G4int nsteps, G4double length, G4int pdgId, G4int name_id){
       Det_ID[id]=det_id;//det_id = 0~5 -> Det_ID = 1~6
       hit_energy[id] = energy;
       hit_startx[id] = start_x;
       hit_starty[id] = start_y;
       hit_startz[id] = start_z;
       hit_timestart[id] = time_start;
       hit_timeend[id] = time_end;
       hit_nsteps[id] = nsteps;
       hit_length[id] = length;
       hit_pdgId[id] = pdgId;
       hit_process[id] = name_id;
    }
    
                                                                                                                     
};

#endif
