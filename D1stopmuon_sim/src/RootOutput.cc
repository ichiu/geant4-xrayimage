#include "RootOutput.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"

RootOutput::RootOutput() {
  TTree::SetMaxTreeSize(100000000000LL);      // Set maximum size of the tree file            
  pointerToRoot=this;
}

RootOutput::~RootOutput() {}

RootOutput* RootOutput::pointerToRoot=0;
RootOutput* RootOutput::GetRootInstance() {
  return pointerToRoot;
}

void RootOutput::BeginOfRunAction() {
   sprintf(RootOutputFileName, "./Output_%s.root", "1");
   rootFile = new TFile(RootOutputFileName, "recreate");
   G4cout << " RE-create file ! " <<G4endl; 
   rootFile->cd();
//   if(rootFile->IsZombie()) { 
//    char message[200];
//    sprintf(message,"musrRootOutput::BeginOfRunAction() Root output file %s can not be created",RootOutputFileName);
//    std::cout << message << std::endl;
//   }else rootFile->cd();

   // ===== muon info. =====
   SciTree = new TTree("tree","Muon information");

   SciTree->Branch("muInitPindex",&muInitPindex,"muInitPindex/I");//init muon 
   SciTree->Branch("muInitP",&muInitP,"muInitP/D");//init muon 
   SciTree->Branch("muInitX",&muInitX,"muInitX/D");//init muon 
   SciTree->Branch("muInitY",&muInitY,"muInitY/D");//init muon 
   SciTree->Branch("muInitZ",&muInitZ,"muInitZ/D");//init muon 
   SciTree->Branch("muInitpX",&muInitpX,"muInitpX/D");//init muon 
   SciTree->Branch("muInitpY",&muInitpY,"muInitpY/D");//init muon 
   SciTree->Branch("muInitpZ",&muInitpZ,"muInitpZ/D");//init muon 
   SciTree->Branch("muInitTime",&muInitTime,"muInitTime/D");//init muon 
   SciTree->Branch("MuonStop_VolumeID",&Stop_Volume,"MuonStop_VolumeID/I");//muon hit

   SciTree->Branch("muFinalPolX",&muFinalPolX,"muFinalPolX/D");
   SciTree->Branch("muFinalPolY",&muFinalPolY,"muFinalPolY/D");
   SciTree->Branch("muFinalPolZ",&muFinalPolZ,"muFinalPolZ/D");
   SciTree->Branch("hit_sample",&hit_sample,"hit_sample/I");

   h1_process = new TH1F("hit_process","Process of Signal",nh1bin,0,nh1bin);
   for (int i=1;i<=nh1bin;i++) h1_process->GetXaxis()->SetBinLabel(i,var_name[i-1]);

   // ===== Energy resolution =====
   reso_init = 0.14896551724137927/1000.;//14keV
   reso_rate = 0.0006769509981851182/1000.;

   G4cout << "RootOutput::BeginOfRunAction()  The Root tree and branches were defined."<<G4endl;
}

void RootOutput::EndOfRunAction() {
  G4cout<<"RootOutput::EndOfRunAction() - Write Tree "<<G4endl;
//  rootTree->Write();
  SciTree->Write();
//  TrackTree->Write();
//  h1_process->Write();
  rootFile->Close();
  G4cout<<"RootOutput::EndOfRunAction() - Root tree written out."<<G4endl;
}

void RootOutput::FillEvent() {
//  double total_E = 0;
//  for (int i = 0; i < nSignals; i++) total_E+=hit_energy[i];
//  if(total_E != 0) rootTree->Fill();//only fill event with energy deposit
  SciTree->Fill();
}

void RootOutput::FillParticle() {
  TrackTree->Fill();
}

//void RootOutput::SetDetectorInfo (G4double edep, G4double edep_e, G4double edep_gamma, G4double edep_other, G4double time){
//  eDep =  edep;
//  eDep_e =  edep_e;
//  eDep_gamma =  edep_gamma;
//  eDep_other =  edep_other;
//  egamma_hit_time =  time;
//}

void RootOutput::SetEnergyResolution (){
   for (int i = 0; i < nSignals; i++){
      hit_energy_reso[i]= G4RandGauss::shoot(hit_energy[i],(reso_init + hit_energy[i]*reso_rate));
   }
}

void RootOutput::ClearAllRootVariables() {
  Stop_Volume = -1000;
  eDep = 0.;eDep_e = 0.; eDep_gamma = 0.; eDep_other = 0.;
  Track_Name = "None";
  Track_Process = "None";
  egamma_hit_time = -1000.;
  runID=-1000;
  eventID=-1000;
  RunTime=-1000;
  for (int j = 0; j < 6; j++){
   muCdTeTime[j] = -1000; muCdTePolX[j] = -1000; muCdTePolY[j] = -1000; muCdTePolZ[j] = -1000;
   muCdTeKineticEnergy[j] = -1000; muCdTeEndKineticEnergy[j] = -1000;
   muCdTeEndTime[j] = -1000; muCdTeEndPolZ[j] = -1000; muCdTeEndPolY[j] = -1000; muCdTeEndPolX[j]  = -1000;
  }
   KineticE = -1000; TotalE = -1000; DepositE = -1000; pdgId = -1000;
   Det_X = -1000; Det_Y = -1000; Det_Z = -1000;
   Ngamma=-1000;
   Neletron=-1000;
   Nneutron=-1000;  
   Nother=-1000;  
  muSampleTime = -1000; muWorldTime = -1000; muKaptonTime = -1000; muTargetTime = -1000;
  muSampleKineticEnergy = -1000; muWorldKineticEnergy = -1000;
  muSamplePolX = -1000; muSamplePolY = -1000; muSamplePolZ = -1000; 
  muKaptonKineticEnergy = -1000; muTargetKineticEnergy = -1000;
  muWorldPolX = -1000; muWorldPolY = -1000; muWorldPolZ = -1000;
  muKaptonPolX = -1000; muKaptonPolY = -1000; muKaptonPolZ = -1000;
  muSampleEndTime = -1000; muWorldEndTime = -1000; muTargetEndTime = -1000;
  muTargetPolX = -1000; muTargetPolY = -1000; muTargetPolZ = -1000;
  muSampleEndKineticEnergy = -1000; muWorldEndKineticEnergy = -1000; muKaptonEndKineticEnergy = -1000; muTargetEndTime = -1000;
  muSampleEndPolX = -1000; muSampleEndPolY = -1000; muSampleEndPolZ = -1000;
  muWorldEndPolX = -1000; muWorldEndPolY = -1000; muWorldEndPolZ = -1000;
  muKaptonEndPolX = -1000; muKaptonEndPolY = -1000; muKaptonEndPolZ = -1000;
  muTargetEndPolX = -1000; muTargetEndPolY = -1000; muTargetEndPolZ = -1000;
  muDecayPolX = -1000; muDecayPolY = -1000; muDecayPolZ = -1000; muDecayTime = -1000;
  muFinalPolX = -1000; muFinalPolY = -1000; muFinalPolZ = -1000;
  Hit_Time = -1000;
  muSampleMomX = -1000; muSampleMomY = -1000; muSampleMomZ = -1000; 
  muWorldMomX = -1000; muWorldMomY = -1000; muWorldMomZ = -1000;
  muKaptonMomX = -1000; muKaptonMomY = -1000; muKaptonMomZ = -1000;
  muSampleEndMomX = -1000; muSampleEndMomY = -1000; muSampleEndMomZ = -1000;
  muWorldEndMomX = -1000; muWorldEndMomY = -1000; muWorldEndMomZ = -1000;
  muKaptonEndMomX = -1000; muKaptonEndMomY = -1000; muKaptonEndMomZ = -1000;
  muTargetEndMomX = -1000; muTargetEndMomY = -1000; muTargetEndMomZ = -1000;
  muTargetMomX = -1000; muTargetMomY = -1000; muTargetMomZ = -1000;
  muInitPindex=-1000; muInitP = -1000; muInitX = -1000; muInitY = -1000;muInitZ = -1000;muInitpX = -1000;muInitpY = -1000;muInitpZ = -1000;  
  eInitX = -1000; eInitY = -1000;eInitZ = -1000;eInitpX = -1000;eInitpY = -1000;eInitpZ = -1000;  
  muInitTime = -1000;

  n_electrons_up = -100;
  n_photons_up = -100; 
  n_electrons_down = -100;
  n_photons_down = -100; 

  for (int i = 0; i < nhitMax; i++){
   hit_energy[i] = 0.;
   hit_energy_reso[i] = 0.;
   hit_timestart[i] = 0.;
   hit_timeend[i] = 0.;
   hit_nsteps[i] = 0;
   hit_length[i] = 0.;
   hit_pdgId[i] = 0;
   hit_process[i] = 0;
  }
}
