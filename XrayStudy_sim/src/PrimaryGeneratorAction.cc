//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "PrimaryGeneratorAction.hh"
#include "Parameters.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "RunAction.hh"

G4int PrimaryGeneratorAction::fractionOfEletronParticles = 10;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0), 
  fParticleGunEle(0), 
  fEnvelopeBox(0)
//  t0(0), tSigma(0), 
//  x0(0), y0(0), z0(-10*CLHEP::cm)
{
  // ------------------------------------------------------------------------
  // get parameter
  // ------------------------------------------------------------------------
  SampleName="Fe";
  if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
     char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
     FILE *fSteeringFile=fopen(charSteeringFileName,"r");
     char  line[501];
     while (!feof(fSteeringFile)) {
        fgets(line,500,fSteeringFile);
        if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
        char tmpString0[100]="Unset", tmpString1[100]="Unset";
        sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
        if (strcmp(tmpString0,"/command/sample")==0){ SampleName = tmpString1;
        std::cout << "here: " << line[0] << " tmpString0 " << tmpString0 << tmpString1 << std::endl;
        }
     }
  }

  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  fParticleGunEle  = new G4ParticleGun(n_particle);

  //define input particles
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle;

  // muon beam
//  particle  = particleTable->FindParticle(particleName="mu-");
//  fParticleGun->SetParticleDefinition(particle);
//  muon_mass = fParticleGun->GetParticleDefinition()->GetPDGMass();

  // gamma source
  particle = particleTable->FindParticle("gamma");
  fParticleGun->SetParticleDefinition(particle);

  G4String eletronName;
  G4ParticleDefinition* eletron
    = particleTable->FindParticle(eletronName="e-");//IH
  fParticleGunEle->SetParticleDefinition(eletron);
  ele_mass = fParticleGunEle->GetParticleDefinition()->GetPDGMass();

  count_event=0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fParticleGunEle;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of ecah event
  //

  // In order to avoid dependence of PrimaryGeneratorAction
  // on DetectorConstruction class we get Envelope volume
  // from G4LogicalVolumeStore.

  RootOutput* myRootOutput = RootOutput::GetRootInstance();
  myRootOutput->ClearAllRootVariables(); 

   /* muon pulse beam
  for (int i=0;i< nPulseBeam; i++){
  // === default particle kinematic ===
  pSigma = p0*mom_error;
  p = G4RandGauss::shoot(p0,pSigma)*MeV;

  // === particle init. position ===
  // ** gauss **
  x0 = G4RandGauss::shoot(poi_mean,poi_sigmaX)*CLHEP::mm;
  y0 = G4RandGauss::shoot(poi_mean,poi_sigmaY)*CLHEP::mm;
  z0 = -50*CLHEP::mm;
  //temp: only for this case (cut for beam)
  //if (std::fabs(x0)>60) x0 = SetCutforBeam(x0,poi_sigmaX);
  //if (std::fabs(y0)>60) y0 = SetCutforBeam(y0,poi_sigmaY);
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));

  // === particle init. momentum and energy ===
  G4double ux = p*dir_error_x*2*(G4UniformRand()-0.5)*MeV,
           uy = p*dir_error_y*2*(G4UniformRand()-0.5)*MeV,
           uz = std::sqrt(p*p - ux*ux - uy*uy)*MeV;
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum
  G4double particleEnergy = std::sqrt(p*p+muon_mass*muon_mass)-muon_mass;
  fParticleGun->SetParticleEnergy(particleEnergy);//IH
  G4double muInitTime = fParticleGun->GetParticleTime()/CLHEP::nanosecond;
  fParticleGun->GeneratePrimaryVertex(anEvent);

  // === electron ===
  G4double ux_e = p*(dir_error_x/10)*2*(G4UniformRand()-0.5)*MeV,
           uy_e = p*(dir_error_y/10)*2*(G4UniformRand()-0.5)*MeV,
           uz_e = std::sqrt(p*p - ux_e*ux_e - uy_e*uy_e)*MeV;
  fParticleGunEle->SetParticleMomentumDirection(G4ThreeVector(ux_e,uy_e,uz_e));
  particleEnergy = std::sqrt(p*p+ele_mass*ele_mass)-ele_mass;
  fParticleGunEle->SetParticleEnergy(particleEnergy);//IH 
  long thisEventNr = (long) (anEvent->GetEventID());
  if ((thisEventNr != 0) && (thisEventNr%fractionOfEletronParticles == 0)) { 
    // ** gauss **
    x0_e = G4RandGauss::shoot(poi_mean,poi_sigmaX)*CLHEP::mm;
    y0_e = G4RandGauss::shoot(poi_mean,poi_sigmaY)*CLHEP::mm;
    //temp: only for this case (cut for beam)
    //if (std::fabs(x0_e)>60) x0_e = SetCutforBeam(x0_e,poi_sigmaX);
    //if (std::fabs(y0_e)>60) y0_e = SetCutforBeam(y0_e,poi_sigmaY);
    fParticleGunEle->SetParticlePosition(G4ThreeVector(x0_e,y0_e,z0));
//    fParticleGunEle->GeneratePrimaryVertex(anEvent);
  }
  myRootOutput->SetInitialMuonParameters(x0,y0,z0,ux,uy,uz,muInitTime);
  myRootOutput->SetInitialEletronParameters(x0_e,y0_e,z0,ux_e,uy_e,uz_e);
  }//Pulse Beam
   // */


//   /* gamma generation
  // === position ===
//  G4double poi_z = -0.5+G4RandGauss::shoot(0.422585,0.0723287)*CLHEP::mm; G4double length_x = 10;G4double length_y = 10;//white
//  G4double poi_z = -0.5+G4RandGauss::shoot(0.433646,0.0777046)*CLHEP::mm; G4double length_x = 20;G4double length_y = 10;//black
//  G4double poi_z = -1.75+G4RandGauss::shoot(0.426187,0.0715852)*CLHEP::mm;G4double length_x = 30;G4double length_y = 10;//dew
//  G4double poi_z = -1.75+G4RandGauss::shoot(1.05532,0.176408)*CLHEP::mm; G4double length_x = 30;G4double length_y = 10;//dew 35
//  G4double poi_z = -0.5*3.28+G4RandGauss::shoot(0.7567,0.1337)*CLHEP::mm; G4double length_x = 8.65; G4double length_y = 5.12;//Ryugu old

  G4double length_x = 0; G4double length_y = 0; G4double length_z = 0;
  G4double mean_z = 0; G4double sigma_z = 0;
   
  if (SampleName == "Ryugu"){
     length_x = 8.65; 
     length_y = 5.12;
     length_z = 3.28;
     mean_z = 0.7148;
     sigma_z = 0.1546;
  }else if (SampleName == "Orguiel"){
     length_x = 5.45; 
     length_y = 5.74;
     length_z = 3.86;
     mean_z = 0.7808;
     sigma_z = 0.1628;
  }else if (SampleName == "Murray"){
     length_x = 6.6+6.8; 
     length_y = 6.6+3;
     length_z = 1.15;
     mean_z = 0.541;
     sigma_z = 0.1131;
  }else if (SampleName == "Yamato"){
     length_x = 6.566+6.177; 
     length_y = 6.566+3;
     length_z = 1.02;
     mean_z = 0.5282;
     sigma_z = 0.1099;
  }else if (SampleName == "Tagishi"){  
     length_x = 6.73; 
     length_y = 6.73;
     length_z = 0.84;
     mean_z = 0.6579;
     sigma_z = 0.1374;
  }else if (SampleName == "Fe"){  
     length_x = 25; 
     length_y = 25;
     length_z = 0.5;
     mean_z = 0.202181;
     sigma_z = 0.0440971;
  }else{
     std::cout << " Wrong sample name !!! " << std::endl;
  }
  G4double poi_z = (-0.5*length_z+G4RandGauss::shoot(mean_z,sigma_z))*CLHEP::mm; 
  while(poi_z > length_z*0.5) poi_z = (-0.5*length_z+G4RandGauss::shoot(mean_z,sigma_z))*CLHEP::mm;

  // === Sample setting ===
  G4double poi_x = (G4UniformRand()-0.5)*2*(length_x/2.)*CLHEP::mm;
  G4double poi_y = (G4UniformRand()-0.5)*2*(length_y/2.)*CLHEP::mm;//(G4UniformRand()-0.5)*2 is -1 ~ 1 | length_x,length_y is size 
  G4double rot_rad=(20./360)*2*CLHEP::pi*CLHEP::rad;
  G4double poi_x_prime=poi_x*std::cos(rot_rad)-poi_z*std::sin(rot_rad);
  G4double poi_z_prime=poi_x*std::sin(rot_rad)+poi_z*std::cos(rot_rad);
  fParticleGun->SetParticlePosition(G4ThreeVector(poi_x_prime,poi_y,poi_z_prime));
  //fParticleGun->SetParticlePosition(G4ThreeVector(0,0,0));
  myRootOutput->h3_init_pos->Fill(poi_x_prime,poi_y,poi_z_prime);

  // === 6 chs direction ===
  G4int idet = count_event%6; // 0~5
  G4double r = 1;
  G4double phi = ((2*CLHEP::pi)/360.)*(60+idet*60+8*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range/360e is +-8 (ratio : 16/360)
  G4double theta = ((2*CLHEP::pi)/360.)*(90+8*(G4UniformRand()-0.5)*2)*CLHEP::rad;//range is +-8 (ratio: 16/180)
  // === 4*pi direction ===
  phi = ((2*CLHEP::pi)/360.)*(G4UniformRand()*360)*CLHEP::rad;//0 ~ 360
  theta = ((2*CLHEP::pi)/360.)*(G4UniformRand()*180)*CLHEP::rad;//0 ~ 180

  G4double ux = r*std::sin(theta)*std::cos(phi);
  G4double uy = r*std::sin(theta)*std::sin(phi);
  G4double uz = r*std::cos(theta);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(ux,uy,uz));//Momentum

  // === random energy ===
  //gen_e_gamma = (30 + 110*G4UniformRand())*0.001;// 30 - 140 keV
  //gen_e_gamma = (Fe_energy[count_event%3])*0.001;// 54, 66 ,125 keV
  gen_e_gamma = (156*0.001);// 54, 66 ,125 keV
  fParticleGun->SetParticleEnergy(gen_e_gamma);
  myRootOutput->h1_init_energy->Fill(gen_e_gamma*1000);
  myRootOutput->h2_init_energy->Fill(gen_e_gamma*1000, idet+1);
  // === specified energy ===
  //G4double gamma_init_e;
  //G4int count_flag=count_event%13;
  //if (count_flag==0)  gamma_init_e = 125.88; //Fegamma
  //if (count_flag==1)  gamma_init_e = 76.7; //Si32
  //if (count_flag==2)  gamma_init_e = 23.1; //Al43
  //if (count_flag==3)  gamma_init_e = 89.3; //Al42
  //if (count_flag==4)  gamma_init_e = 42.9; //Fe54
  //if (count_flag==5)  gamma_init_e = 92.9; //Fe43
  //if (count_flag==6)  gamma_init_e = 54.7; //Ca43
  //if (count_flag==7)  gamma_init_e = 159.6; //Ca32
  //if (count_flag==8)  gamma_init_e = 56.4; //Mg32
  //if (count_flag==9)  gamma_init_e = 115.7; //Cu43
  //if (count_flag==10) gamma_init_e = 133.6; //O21
  //if (count_flag==11) gamma_init_e = 158.5; //O31
  //if (count_flag==12) gamma_init_e = 167.5; //O41
  //fParticleGun->SetParticleEnergy(gamma_init_e*0.001);
  //myRootOutput->h1_init_count->Fill(count_flag);


  fParticleGun->GeneratePrimaryVertex(anEvent);
  count_event++;

  // */

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

