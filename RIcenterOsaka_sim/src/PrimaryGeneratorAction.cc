//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//

#include "PrimaryGeneratorAction.hh"
#include "Parameters.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "RunAction.hh"
#include "G4GeneralParticleSource.hh"
#include "G4Geantino.hh"
#include "G4IonTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0)
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // *** RI gen. ***
  char tmpString1[100]="Unset", tmpString0[100]="Unset";
  rot_angle=90;
  if (strcmp(Parameters::mySteeringFileName,"Unset")!=0){
     char charSteeringFileName[1000]; strcpy(charSteeringFileName,(Parameters::mySteeringFileName).c_str());
     FILE *fSteeringFile=fopen(charSteeringFileName,"r");
     char  line[501];
     while (!feof(fSteeringFile)) {
        fgets(line,500,fSteeringFile);
        if ((line[0]=='#')||(line[0]=='\n')||(line[0]=='\r')) continue;
        sscanf(&line[0],"%s %s",tmpString0,tmpString1);//command, sample name
        if (strcmp(tmpString0,"/command/rot")==0)rot_angle = std::stoi(tmpString1);
     }
  }

  // *** gamma gen ***
//  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
//  G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
//  fParticleGun->SetParticleDefinition(particle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  RootOutput* myRootOutput = RootOutput::GetRootInstance();
  myRootOutput->ClearAllRootVariables(); 

  // *** RI gen. ***
  if (fParticleGun->GetParticleDefinition() == G4Geantino::Geantino()) {
     G4int Z = 27, A = 57;//Co57
     G4double excitEnergy = 0.*keV;
     G4ParticleDefinition* ion = G4IonTable::GetIonTable()->GetIon(Z,A,excitEnergy);
     fParticleGun->SetParticleDefinition(ion);
  }
  fParticleGun->SetParticlePosition(G4ThreeVector(100*std::sin((rot_angle/360.)*2*CLHEP::pi*CLHEP::rad),0, 100*(std::cos((rot_angle/360.)*2*CLHEP::pi*CLHEP::rad)-1)*CLHEP::mm));
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
  fParticleGun->SetParticleCharge(0.*eplus);
  fParticleGun->SetParticleEnergy(0*eV);
  fParticleGun->GeneratePrimaryVertex(anEvent);

  // *** gamma gen. ***
//  gen_e_gamma = G4UniformRand()*200*0.001;
//  fParticleGun->SetParticlePosition(G4ThreeVector(0,0,0*CLHEP::mm));
//  fParticleGun->SetParticleEnergy(gen_e_gamma);//random 0~200 [keV]
//  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(2*(G4UniformRand()-0.5)*MeV,2*(G4UniformRand()-0.5)*MeV,2*(G4UniformRand()-0.5)*MeV));//4*pi 
//  fParticleGun->GeneratePrimaryVertex(anEvent);
//  myRootOutput->h1_init_energy->Fill(gen_e_gamma*1000);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

